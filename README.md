# Ssam's OpenQA Tool

This is a small commandline helper for working with the test tool OpenQA. I use it for working with GNOME's OpenQA tests.

## Installing

Prebuilt binaries of `ssam_openqa` are available from the projects [package registry](https://gitlab.gnome.org/sthursfield/ssam_openqa/-/packages). (If these aren't suitable, see below for how to build from source). Install the binary into your executable PATH, for example into `$HOME/.local/bin`.

## Running the example testsuite

There is a minimal operating system ISO and some example openQA testsuites
available in `examples/minimal-test`.

You can run the `minimal_test` testsuite as follows:

    ssam_openqa run --tests-path . --output-path ./out --iso-path buildroot-minimal.iso --testsuites minimal_test

This will attempt to start the tests in a Podman or Docker container, and on
completion will copy the test results to `./out/`.

There are additional testsuites which deliberately fail or hang indefinitely,
which you can use to test the interactive debugging capabilities of
`ssam_openqa`.

## Running real world testsuites

The openQA testsuite for GNOME OS can be run using `ssam_openqa`. For instructions, see the
README guide in: <https://gitlab.gnome.org/gnome/openqa-tests>.

## Ensuring fast tests

If your computer has a 'Power Mode' setting, ensure it is set to 'Performance' rather than 'Power Saver' to avoid tests
running slower than necessary.

## Building from source

You can build and install `ssam_openqa` from source using a Rust compiler. For example:

    git clone ...
    cargo build --release
    cp ./target/release/ssam_openqa ~/.local/bin

## Running the testsuite

There is a suite of integration tests. These run the `ssam_openqa` program to verify its behaviour. Some tests will actually trigger openQA on a minimal ISO image. These tests are ignored by default, you can run the full test suite as follows:

    cargo test -- --include-ignored

By default the tests don't output anything to stdout. In the "real scenario"
tests, look for lines marked `/* Show full process output */`, you can uncomment
these to show the status messages from the child process.

Note the Rust test framework will hide output from the tests unless you pass
`--show-output` to the test.
