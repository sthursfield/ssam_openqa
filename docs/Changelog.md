Unreleased
----------

  * Display more details when a test fails. Rather than just `test failed` you might now see:

        gnome_apps::app_gnome_calculator: Test failed.
          needle: no matching needle with tag app_gnome_calculator_home
          screenshot: app_gnome_calculator-3.png
          candidates: 7

  * Dependency updates

  * Improvements to the integration tests.

1.3.1
-----

  * Fix busyloop in the frontend process that was causing 100% CPU usage (!!!).

1.3.0
-----

  * Support multiple hard disk assets (`HDD_1`, `HDD_2`, etc.). You need to pass
    `--hdd-path` multiple times when running a test that requires multiple hard disks.

  * Use latest openQA worker image (15.6 base)

  * Fix an issue where CTRL+C occasionally killed the process instead of opening the pause handler
    ([issue 18](https://gitlab.gnome.org/sthursfield/ssam_openqa/-/issues/18)).

  * Internal changes:

     * Use [tungstenite](https://github.com/snapview/tungstenite-rs) to communicate with
       websockets instead of [Rust-WebSocket](https://github.com/websockets-rs/rust-websocket).
     * Use [process-wrap](https://github.com/watchexec/process-wrap) instead of
       [command-group](https://github.com/watchexec/command-group).
     * Reliability improvements in error situations.
     * Update dependency versions and remove support for Rustc 1.72

1.2.0
-----

  * Add `(t)erminal` option to the pause prompt, which provides a basic connection to any
    virtio terminal provided by QEMU.

  * Add `examples/minimal-test`, which boots a real 6MB .iso image included in this repo.

  * Improvements in how the frontend shows test failures.

  * Show important messages from isotovideo log file when each testsuite
    finishes.

  * Removals:

      * The `pause at (t)est` option in the pause prompt is removed. You can still use
        `--pause-test` on the commandline to set this.

  * Internal changes:

      * New integration tests built around `examples/minimal-test`
      * Use `serde_json` for all JSON parsing, remove an additional dependency on `json` crate

1.1.0
-----

  * Add `--frontend` option and two options of frontend:

     * `interactive`: terminal UI designed for interactive use, built with
       [indicatif](https://docs.rs/indicatif/latest/indicatif/)
     * `json`: non-interactive frontend that outputs status as JSON lines.

  * Sync the test output to the host while the tests run (using a bind-mount),
    rather than copying it out once at the end.

  * Support using Docker to run containers in addition to Podman

  * Allow changing pause settings while tests are running from the
    interactive 'paused' prompt.

1.0.0
-----

  * Initial release.
