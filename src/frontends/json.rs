//! Frontend intended for scripted / non-interactive use that outputs JSON to stdout.

use std::io::{self, Write};
use std::sync::mpsc;

use log::*;

use crate::backend::BackendEvent;
use super::FrontendEvent;

fn setup_ctrlc_handler(tx: mpsc::Sender::<FrontendEvent>) -> Result<(), anyhow::Error> {
    ctrlc::set_handler(move || {
        debug!("CTRL+C received. Sending UserAbort event to runner...");
        tx.send(FrontendEvent::UserAbort)
            .unwrap_or_else(|err| error!("Failed to send CTRL+C to main loop: {}", err));
    })?;
    Ok(())
}

pub fn main_loop(frontend_event_tx: mpsc::Sender<FrontendEvent>, runner_event_rx: mpsc::Receiver<BackendEvent>) -> Result<(), anyhow::Error> {
    setup_ctrlc_handler(frontend_event_tx)?;

    let mut stdout = io::stdout();
    loop {
        let message = runner_event_rx.recv()
            .expect("Backend should send ProgramExit event before closing its message channel");

        stdout.write_all(serde_json::to_string(&message)?.as_bytes())?;
        stdout.write_all(b"\n")?;

        if let BackendEvent::ProgramExit { .. } = message {
            break;
        }
    };

    stdout.flush()
        .unwrap_or_else(|_| warn!("Failed to flush stdout"));

    Ok(())
}
