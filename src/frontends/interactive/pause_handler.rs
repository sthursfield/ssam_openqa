//! The interactive pause prompt that shows when CTRL+C is pressed.

use std::io::{self, Write};

use anyhow::bail;
use log::*;

use crate::UserConfig;

/// User input returned by the interactive pause handler.
pub enum PauseHandlerResponse {
    Invalid,
    Abort,
    ChangePauseOnEvent,
    Continue,
    Terminal(String),
    Vnc
}

pub fn run_paused_handler(user_config: &UserConfig) -> Result<PauseHandlerResponse, anyhow::Error> {
    println!();
    println!("Enter one of the following commands:");
    println!("  (a)bort - Abort testing and exit");
    println!("  (c)ontinue - Continue testing");
    println!("  (p)ause-on-event - Change 'pause-on-event' setting (currently: {})", user_config.pause_on_event);
    println!("  (t)erminal - Interact with the test virtual machine using a virtio console");
    println!("  (v)nc - Show VNC connection info for the test virtual machine");
    println!();

    print!("Choice:");
    io::stdout().flush().unwrap();

    let mut buffer = String::new();
    let stdin = io::stdin();

    // FIXME: CTRL+C will be ignored while we wait for input.
    // See: https://github.com/Detegr/rust-ctrlc/issues/30 for why.
    match stdin.read_line(&mut buffer) {
        Ok(_size) => {
            let buffer_trimmed = buffer.strip_suffix('\n')
                .unwrap_or("");

            if !buffer_trimmed.is_empty() {
                let command_words: Vec<&str> = buffer_trimmed.split_ascii_whitespace().collect();
                let command = command_words[0];
                debug!("Got pause action: '{}'", buffer_trimmed);
                if "abort".starts_with(command) {
                    return Ok(PauseHandlerResponse::Abort)
                }
                if "continue".starts_with(command) {
                    return Ok(PauseHandlerResponse::Continue)
                }
                if "pause-on-event".starts_with(command) {
                    return Ok(PauseHandlerResponse::ChangePauseOnEvent)
                }
                if "terminal".starts_with(command) {
                    return match get_command_arg(buffer_trimmed) {
                        Ok(arg) => {
                            let terminal_name = arg.unwrap_or("virtio_console".to_string());
                            Ok(PauseHandlerResponse::Terminal(terminal_name))
                        },
                        Err(err) => {
                            println!("Invalid args for terminal command: {}", err);
                            Ok(PauseHandlerResponse::Invalid)
                        },
                    };
                }
                if "vnc".starts_with(command) {
                    return Ok(PauseHandlerResponse::Vnc)
                }
            }
        },
        Err(e) => {
            bail!(e)
        }
    }
    Ok(PauseHandlerResponse::Invalid)
}

fn get_command_arg(buffer: &str) -> Result<Option<String>, anyhow::Error> {
    let parts: Vec<&str> = buffer.split_ascii_whitespace().collect();
    if parts.len() < 2 {
        Ok(None)
    } else if parts.len() == 2 {
        Ok(Some(parts[1].to_string()))
    } else {
        bail!("`terminal` accepts at most one argument")
    }
}

