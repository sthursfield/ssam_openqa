//! Simulate a terminal connection to the VM.
//!
//! QEMU can create FIFOs to read and write from virtio-terminal devices.
//! We use this to create a kind of terminal connection to the VM, for debugging.

use std::fs::File;
use std::io::prelude::*;
use std::io::stdout;
use std::path::PathBuf;
use std::sync::mpsc::{self, TryRecvError};
use std::thread;

use anyhow::Context;
use console::{Term, Key};
use log::*;

use crate::OutputDirectory;

// This combo is also used by openSSH to disconnect from remote machines.
pub const ESCAPE_CHAR: char = '~';
pub const EXIT_COMMAND: char = '.';

enum ReaderMessage {
    Stop
}

pub struct TerminalConnection {
    input_pipe: PathBuf,
    reader_tx: mpsc::Sender<ReaderMessage>,
}

pub enum TerminalAction {
    Continue,
    Disconnect,
}

// Thread to read from the virtio console's output FIFO and echo each new char to the screen.
fn start_reader(output_pipe: PathBuf) -> Result<mpsc::Sender<ReaderMessage>, anyhow::Error> {
    let (tx, rx) = mpsc::channel();
    let mut f = File::open(&output_pipe)?;

    thread::spawn(move || {
        debug!("Started terminal reader thread, reading: {}", output_pipe.display());

        loop {
            trace!("Loop in terminal reader thread");

            let mut buffer = [0; 1];

            // We have to process one byte at a time. Reading to EOL is not useful, because
            // the read may block in the middle of a prompt like "Password:".
            let read_result = f.read_exact(&mut buffer);
            if let Err(e) = read_result {
                debug!("Error reading {}: {}. Disconnecting terminal reader", output_pipe.display(), e);
                break;
            }

            // Check if the terminal already exited before printing the next byte
            // to avoid "ghost" output appearing after the "Stop" message was sent.
            match rx.try_recv() {
                Ok(ReaderMessage::Stop) => break,
                Err(TryRecvError::Empty) => {},
                Err(error) => {
                    debug!("Exiting terminal reader: {}", error);
                    break;
                }
            }

            let write_result = stdout().write(&buffer);
            if let Err(e) = write_result {
                debug!("Write error: {e}");
            }
            let flush_result = stdout().flush();
            if let Err(e) = flush_result {
                debug!("Flush error: {e}");
            }
        }
    });

    Ok(tx)
}

impl TerminalConnection {
    pub fn connect(output: &OutputDirectory, testsuite_name: &str, terminal_name: &str) -> Result<Self, anyhow::Error> {
        let input_pipe = output.get_virtio_console_input(testsuite_name, terminal_name)?;
        let output_pipe = output.get_virtio_console_output(testsuite_name, terminal_name)?;
 
        let reader_tx = start_reader(output_pipe)?;

        let result = TerminalConnection {
            input_pipe,
            reader_tx,
        };
        Ok(result)
    }

    /// Read chars from unbuffered stdio and write them to the virtio console.
    ///
    /// Detects new-line and returns to caller, with [TerminalAction::Continue].
    ///
    /// Detects ESCAPE_CHAR and EXIT_COMMAND to return to caller, with TerminalAction::Disconnect.
    /// On read error or write error, TerminalAction::Disconnect is also returned immediately.
    pub fn run_prompt(&mut self) -> TerminalAction {
        let mut term = Term::stdout();

        let mut f = match File::create(&self.input_pipe) {
            Ok(f) => f,
            Err(e) => {
                warn!("Unable to open input FIFO {}: {}", self.input_pipe.display(), e);
                return TerminalAction::Disconnect;
            }
        };

        match self.process_line(&mut term, &mut f) {
            Ok(action) => action,
            Err(e) => {
                debug!("Error: {}", e);
                TerminalAction::Disconnect
            }
        }
    }

    fn process_line(&mut self, stdin: &mut Term, virtio_out: &mut dyn Write) -> Result<TerminalAction, anyhow::Error> {
        let mut first: bool = true;

        loop {
            trace!("Loop in terminal line reader");

            let mut key = stdin.read_key()
                .with_context(|| "Read error")?;

            if first && key == Key::Char(ESCAPE_CHAR) {
                let escape_command = stdin.read_key()
                    .with_context(|| "Read error")?;

                if escape_command == Key::Char(EXIT_COMMAND) {
                    return Ok(TerminalAction::Disconnect);
                }

                self.send_key(virtio_out, &key)?;
                key = escape_command;
            }

            self.send_key(virtio_out, &key)?;

            if key == Key::Enter || key == Key::Char('\n') {
                return Ok(TerminalAction::Continue);
            }

            first = false;
        }
    }

    fn send_key(&self, out: &mut dyn Write, key: &Key) -> Result<(), anyhow::Error> {
        let mut char_buffer = [0; 4];

        let data = match key {
            Key::Char(c) => c.encode_utf8(&mut char_buffer).as_bytes(),
            Key::Backspace => &[0x08],
            Key::Del => &[0x7F],
            Key::Enter => &[0x0A],
            Key::Tab => &[0x09],
            _ => &char_buffer[0..0],
        };
        let _size = out.write(data)?;
        Ok(())
    }


    pub fn disconnect(&self) {
        self.reader_tx.send(ReaderMessage::Stop)
            .unwrap_or_else(|e| debug!("Couldn't stop reader thread: {}", e));
        debug!("Send ReaderMessage::Stop");
    }
}
