//! Frontend for human / interactive use.
//!
//! Built around progress bars from the library
//! [indicatif](https://github.com/console-rs/indicatif/).

mod pause_handler;
mod terminal;
mod vnc;

use std::sync::mpsc::{self, TryRecvError};
use std::thread;
use std::time::{Duration, Instant};

use indicatif::{ProgressBar, ProgressDrawTarget, ProgressStyle};
use log::*;

use crate::helpers::isotovideo::{self, IsotovideoStatusMessage};
use crate::helpers::isotovideo::test_result::StepResult;
use crate::backend::BackendEvent;
use crate::{ContainerRuntime, OutputDirectory, TestStatus};
use crate::UserConfig;
use super::FrontendEvent;

use pause_handler::PauseHandlerResponse;
use terminal::{TerminalAction, TerminalConnection, ESCAPE_CHAR, EXIT_COMMAND};

/// Possible states for the frontend.
#[derive(Clone, Copy, PartialEq)]
enum State {
    Start,
    Run,
    Pause,
    PauseWithTerminal,
    NextTestsuite,
    Exit,
}

pub struct InteractiveFrontend {
    crun: ContainerRuntime,
    testsuites: Vec<String>,
    output: OutputDirectory,
    frontend_event_tx: mpsc::Sender<FrontendEvent>,
    runner_event_rx: mpsc::Receiver<BackendEvent>,

    state: State,

    bar: ProgressBar,

    container_name: Option<String>,
    testsuite_index: usize,
    testsuite_name: String,
    test_order: Vec<String>,
    test_index: Option<usize>,

    user_config: UserConfig,
}

/// How often the event loop wakes up to check events.
const TICK_INTERVAL_MS: u64 = 500;

/// Spinner for when a testsuite is starting, before the test_order.json file is loaded.
fn loading_spinner(testsuite_name: &str) -> ProgressBar {
    let style = ProgressStyle::with_template("{prefix}: {spinner} {msg}")
        .unwrap();

    let bar = ProgressBar::new_spinner()
        .with_prefix(testsuite_name.to_string())
        .with_message("Starting")
        .with_style(style);
    bar.enable_steady_tick(Duration::from_millis(500));
    bar
}

/// Progress bar for once we have a test order.
fn progress_bar(testsuite_name: &str, test_order: &[String]) -> ProgressBar {
    let style = ProgressStyle::with_template("{prefix}::{wide_msg} {bar} {elapsed_precise}")
        .unwrap();

    ProgressBar::new(test_order.len() as u64)
        .with_style(style)
        .with_prefix(testsuite_name.to_string())
}

fn hide_bar(bar: &mut ProgressBar) {
    bar.set_draw_target(ProgressDrawTarget::hidden());
}

fn show_bar(bar: &mut ProgressBar) {
    bar.set_draw_target(ProgressDrawTarget::stderr());
    bar.tick();
}

fn summarize_failure(step: StepResult) -> String {
    match step {
        StepResult::AssertScreenResult { tags, needles, screenshot, .. } => {
            let tag_list = tags.join(",");
            let tag_or_tags = if tags.len() > 1 { "tags" } else { "tag" };
            let n_candidates = needles.len();
            format!("  needle: no matching needle with {tag_or_tags} {tag_list}\n  screenshot: {screenshot}\n  candidates: {n_candidates}")
        },
        StepResult::AudioCapture { audio, result, .. } => {
            format!("  audio capture: result {result}, see {audio}")
        },
        StepResult::GenericResult { title, text, .. } => {
            format!("  {title}: see {text}")
        },
        StepResult::Screenshot { screenshot, result, .. } => {
            format!("  screenshot: result {result}, see {screenshot}")
        },
    }
}


impl InteractiveFrontend {
    pub fn new(
        crun: ContainerRuntime,
        testsuites: Vec<String>,
        output: OutputDirectory,
        user_config: UserConfig,
        frontend_event_tx: mpsc::Sender<FrontendEvent>,
        runner_event_rx: mpsc::Receiver<BackendEvent>
    ) -> Self {
        let first_testsuite = testsuites[0].clone();
        InteractiveFrontend {
            crun,
            testsuites,
            output,

            frontend_event_tx,
            runner_event_rx,

            state: State::Start,

            bar: loading_spinner(&first_testsuite),

            container_name: None,
            testsuite_index: 0,
            testsuite_name: first_testsuite,
            test_order: vec!(),
            test_index: None,

            user_config,
        }
    }

    fn index_of_test(&self, name: &str) -> Option<usize> {
        (0..self.test_order.len()).find(|&i| self.test_order[i] == name)
    }

    fn prepare_next_testsuite(&mut self) {
        self.test_order = vec!();
        self.test_index = None;
        self.testsuite_name.clone_from(&self.testsuites[self.testsuite_index]);
        self.bar = loading_spinner(&self.testsuite_name);
    }

    pub fn main_loop(&mut self) -> Result<(), anyhow::Error> {
        self.bar.tick();

        self.setup_ctrlc_handler()?;

        let mut tick = Instant::now();
        let mut next_tick = tick + Duration::from_millis(TICK_INTERVAL_MS);

        let mut terminal_connection: Option<TerminalConnection> = None;

        while self.state != State::Exit {
            self.bar.tick();

            loop {
                trace!("Loop in interactive matching runner events");
                match self.runner_event_rx.try_recv() {
                    Ok(message) => {
                        self.state = self.handle_event(&message);
                        if self.state == State::Exit {
                            break;
                        }
                    }
                    Err(TryRecvError::Empty) => break,
                    Err(_error) => {
                        panic!("Backend closed its channel without sending ProgramExit");
                    },
                }
            }

            if self.state == State::NextTestsuite {
                self.show_isotovideo_logs()
                    .unwrap_or_else(|e| eprintln!("Failed to show isotovideo error output: {e}"));

                self.testsuite_index += 1;
                if self.testsuite_index >= self.testsuites.len() {
                    /* We finished the last testsuite */
                    info!("Reached testsuite {}/{}, exiting frontend", self.testsuite_index, self.testsuites.len());
                    self.state = State::Exit;
                    break;
                };
                self.prepare_next_testsuite();

                self.state = State::Run;
            }

            if self.state == State::Pause {
                debug!("Pause here");
                /* Await user input - we ignore test runner events and CTRL-C here */
                match pause_handler::run_paused_handler(&self.user_config) {
                    Ok(PauseHandlerResponse::Abort) => {
                        self.frontend_event_tx.send(FrontendEvent::UserAbort)
                            .unwrap_or_else(|err| error!("Failed to send UserAbort message to main loop: {}", err));
                        self.state = State::Run;
                    },
                    Ok(PauseHandlerResponse::Continue) => {
                        self.frontend_event_tx.send(FrontendEvent::UserContinue)
                            .unwrap_or_else(|err| error!("Failed to send UserContinue message to main loop: {}", err));
                        self.state = State::Run;
                    },
                    Ok(PauseHandlerResponse::ChangePauseOnEvent) => {
                        self.user_config.pause_on_event = self.user_config.pause_on_event.next();
                        self.frontend_event_tx.send(FrontendEvent::UserChangeConfig(self.user_config.clone()))
                            .unwrap_or_else(|err| error!("Failed to send UserChangeConfig message to main loop: {}", err));
                    },
                    Ok(PauseHandlerResponse::Terminal(terminal_name)) => {
                        let conn_result = TerminalConnection::connect(&self.output, &self.testsuite_name, &terminal_name);
                        if let Err(e) = conn_result {
                            eprintln!("Error connecting to virtio console `{terminal_name}`: {e}.");
                            eprintln!("Check the VIRTIO_TERMINAL setting in the machine config.");
                        } else {
                            println!("Connecting to virtio console `{terminal_name}`. Type {ESCAPE_CHAR}{EXIT_COMMAND} to disconnect.");
                            terminal_connection = Some(conn_result.unwrap());
                            self.state = State::PauseWithTerminal;
                        }
                    },
                    Ok(PauseHandlerResponse::Vnc) => {
                        if let Some(container_name) = &self.container_name {
                            vnc::show_vnc_connection_info(&self.crun, container_name);
                        } else {
                            println!("Container is not currently running.");
                        }
                    },
                    Ok(PauseHandlerResponse::Invalid) => {
                        debug!("Invalid pause response");
                    },
                    Err(e) => {
                        eprintln!("{}", e);
                    }
                }
            }

            if self.state == State::PauseWithTerminal {
                let action = terminal_connection
                    .as_mut()
                    .expect("in PauseWithTerminal state")
                    .run_prompt();
                match action {
                    TerminalAction::Continue => {},
                    TerminalAction::Disconnect => {
                        println!("Disconnecting terminal");
                        terminal_connection.unwrap().disconnect();
                        terminal_connection = None;
                        self.state = State::Pause;
                        debug!("Disconn");
                    }
                }
            }

            let now = Instant::now();
            if now < next_tick {
                let until_next_tick: Duration = next_tick - now;
                thread::sleep(until_next_tick);
            }

            tick = Instant::now();
            next_tick = tick + Duration::from_millis(TICK_INTERVAL_MS);
        };

        Ok(())
    }

    fn setup_ctrlc_handler(&self) -> Result<(), anyhow::Error> {
        let tx = self.frontend_event_tx.clone();
        ctrlc::set_handler(move || {
            debug!("CTRL+C received. Sending UserPause event to runner...");
            tx.send(FrontendEvent::UserPause)
                .unwrap_or_else(|err| error!("Failed to send pause message to main loop: {}", err));
        })?;

        Ok(())
    }

    fn handle_event(&mut self, event: &BackendEvent) -> State {
        info!("Event from main loop: {:?}", event);

        match event {
            BackendEvent::InternalError(error) => {
                self.bar.abandon_with_message("Internal error");
                eprintln!("Internal error: {}.", error);
                State::Exit
            },

            BackendEvent::IsotovideoContainerStart(name) => {
                self.container_name = Some(name.clone());
                self.state
            }

            BackendEvent::IsotovideoContainerExit(_exit_code) => {
                self.state
            },

            BackendEvent::IsotovideoStatus(status) => {
                match status {
                    IsotovideoStatusMessage::CurrentTest { name, full_name: _full_name } => {
                        if name.is_some() {
                            let maybe_index = self.index_of_test(name.as_ref().unwrap());
                            if let Some(index) = maybe_index {
                                self.test_index = Some(index);
                                self.bar.set_position(index as u64);
                                self.bar.set_message(name.as_ref().unwrap().to_string());
                            }
                        }
                        self.state
                    },
                    IsotovideoStatusMessage::CurrentApiFunction { function } => {
                        if self.test_index.is_some() {
                            let test_name = &self.test_order[self.test_index.unwrap()];
                            self.bar.set_message(format!("{} ({})", test_name, function));
                        };
                        self.state
                    },
                    IsotovideoStatusMessage::Paused { reason } => {
                        if reason.is_some() {
                            println!("Testing paused, due to: {}", reason.as_ref().unwrap());
                        } else {
                            println!("Testing paused");
                        }
                        hide_bar(&mut self.bar);
                        State::Pause
                    },
                    IsotovideoStatusMessage::ResumeTestExecution => {
                        show_bar(&mut self.bar);
                        State::Run
                    }
                    _ => self.state,
                }
            },

            BackendEvent::ProgramExit { testsuite_result: _result, error: _error } => {
                State::Exit
            }

            BackendEvent::TestOrder(order) => {
                self.test_order.clone_from(order);
                self.bar.finish_and_clear();

                self.bar = progress_bar(&self.testsuite_name, &self.test_order);
                self.state
            }

            BackendEvent::TestResult { test_name, test_result } => {
                if test_result.status() == TestStatus::Fail {
                    let summary = if let Some(first_failure) = test_result.first_failure() {
                        summarize_failure(first_failure)
                    } else {
                        "no details".to_string()
                    };
                    self.bar.println(
                        format!("{}::{test_name}: Test failed.\n{summary}", self.testsuite_name)
                    )
                };
                self.state
            }

            BackendEvent::TestsuiteResult { testsuite_result } => {
                if *testsuite_result == TestStatus::Pass {
                    self.bar.finish_with_message("Testsuite passed");
                } else {
                    self.bar.finish_with_message("Testsuite failed");
                }

                State::NextTestsuite
            }

            BackendEvent::TestsuiteBroken => {
                self.bar.finish_with_message("Failed to load testsuite");

                self.show_isotovideo_logs()
                    .unwrap_or_else(|e| eprintln!("Failed to show isotovideo error output: {e}"));

                State::Exit
            }
        }
    }

    fn show_isotovideo_logs(&self) -> Result<(), anyhow::Error> {
        let log = self.output.get_isotovideo_log_for_testsuite(&self.testsuite_name)?;
        let log_messages = isotovideo::get_isotovideo_log_highlights(log.as_path())?;

        println!("\n");
        if log_messages.is_empty() {
            println!("Error log: {}", log.display());
        } else {
            println!("Highlights from isotovideo log:");
            for log_message in log_messages {
                println!("  {}", log_message.text);
            }
            println!("Full stderr output: {}", log.display());
        }
        println!();

        Ok(())
    }
}
