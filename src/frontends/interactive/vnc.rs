//! VNC specific utilities.

use crate::backend;
use crate::ContainerRuntime;

use log::*;

/// The ssam_openqa tool doesn't support VNC directly, instead we show a hint on
/// how to connect to the system under test using a VNC client.
pub fn show_vnc_connection_info(crun: &ContainerRuntime, container_name: &str) {
    match backend::get_vnc_port(crun, container_name) {
        Ok(port) => {
            println!();
            println!("VNC connection available at localhost:{}", port);
            println!();
            println!("To connect using TigerVNC:");
            println!();
            println!("    vncviewer localhost:{} -Shared -ViewOnly", port);
        },
        Err(err) => {
            error!("Failed to get VNC port: {}", err);
        },
    }
}
