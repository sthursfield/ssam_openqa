/// Utility functions for frontends.

use std::collections::VecDeque;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

/// Return last N lines of log file
pub fn last_n_lines(path: &Path, n_lines: usize) -> Result<VecDeque<String>, anyhow::Error> {
    let file = File::open(path)?;
    let mut reader = BufReader::new(file);
    let mut buf = String::new();
    let mut lines: VecDeque<String> = VecDeque::with_capacity(n_lines);

    loop {
        trace!("Loop reading log file");
        let line_len = reader.read_line(&mut buf)?;

        if line_len == 0 {
            break;
        };

        if lines.len() >= n_lines {
            lines.pop_front();
        }

        lines.push_back(buf.clone());

        buf.clear();
    };

    Ok(lines)
}
