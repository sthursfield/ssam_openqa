//! User interface modules.

mod event;
mod interactive;
mod json;

use std::str::FromStr;
use std::sync::mpsc;
use std::thread::{self, JoinHandle};

use anyhow::bail;

use crate::ContainerRuntime;
use crate::OutputDirectory;
use crate::backend::BackendEvent;
use crate::UserConfig;
pub use event::FrontendEvent;
use interactive::InteractiveFrontend;

#[derive(Debug, Default, PartialEq)]
pub enum FrontendType {
    #[default]
    Interactive,
    Json,
}

impl FromStr for FrontendType {
    type Err = anyhow::Error;

    fn from_str(input: &str) -> Result<FrontendType, anyhow::Error> {
        match input {
            "interactive" => Ok(FrontendType::Interactive),
            "json" => Ok(FrontendType::Json),
            _ => bail!("Unknown frontend type: {}.", input)
        }
    }
}

pub fn start(
    frontend_type: &FrontendType,
    crun: &ContainerRuntime,
    testsuites: Vec<String>,
    output: OutputDirectory,
    user_config: UserConfig)
-> (JoinHandle<Result<(), anyhow::Error>>, mpsc::Receiver<FrontendEvent>, mpsc::Sender<BackendEvent>) {
    let (runner_event_tx, runner_event_rx) = mpsc::channel();
    let (frontend_event_tx, frontend_event_rx) = mpsc::channel();

    let crun = crun.clone();

    let handle = match frontend_type {
        FrontendType::Interactive => thread::spawn(move || {
            InteractiveFrontend::new(crun, testsuites, output, user_config, frontend_event_tx, runner_event_rx)
                .main_loop()
        }),
        FrontendType::Json => thread::spawn(move || {
            json::main_loop(frontend_event_tx, runner_event_rx)
        }),
    };

    (handle, frontend_event_rx, runner_event_tx)
}
