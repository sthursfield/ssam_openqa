//! Events published by frontend.

use crate::UserConfig;

/// Events that are processed by the runner main loop.
#[derive(Debug)]
pub enum FrontendEvent {
    /// User aborted the test run.
    UserAbort,
    /// User continued tests after pause.
    UserContinue,
    /// User requested change to runtime settings.
    UserChangeConfig(UserConfig),
    /// User requested the test runner pause.
    UserPause,
}
