//! Sam's OpenQA helper tool.
//!
//! This is a commandline tool written for use with the GNOME OpenQA tests.
//! It supports running the OpenQA tests locally inside a Podman or Docker container.
//!
//! Subcommands:
//!
//!   * `run`: Run the provided tests on a given ISO image.

mod backend;
mod helpers;
mod frontends;
mod types;
mod user_config;

use anyhow::{anyhow, bail, Context};
use arg::Args;
use log::*;
use process_wrap::std::ProcessGroup;

use std::collections::HashMap;
use std::env;
use std::path::{Path, PathBuf};
use std::process::ExitCode;
use std::sync::mpsc;

use crate::backend::{Backend, BackendEvent};
use crate::frontends::{FrontendEvent, FrontendType};
use crate::helpers::{OutputDirectory, ScenarioDefinitions};
use crate::helpers::container::{ContainerRuntime, VolumeSpec};
use crate::helpers::isotovideo::IsotovideoConfig;
use crate::types::{PauseEvent, TestStatus};
use crate::user_config::UserConfig;


#[derive(Args, Debug)]
/// run
/// Run a specific test suite against an OS image.
struct CommandRun {
    #[arg(short = "f", long = "frontend")]
    ///Set frontend to use: `interactive`, `json`
    frontend: FrontendType,

    #[arg(short = "h", long = "hdd-path")]
    ///Path of hard disk to test.
    hdd_path: Vec<String>,

    #[arg(short = "i", long = "iso-path")]
    ///Path to ISO to test.
    iso_path: String,

    #[arg(short = "a", long = "asset-path")]
    ///Path to test asset. (Can be specified multiple times).
    asset_path: Vec<String>,

    #[arg(short = "p", long = "tests-path", required)]
    ///Path to tests repo.
    ///
    ///Needles are expected to be in a `needles/` directory inside this repo.
    tests_path: String,

    #[arg(short = "o", long = "output-path", required)]
    ///Path to place test output.
    output_path: String,

    #[arg(short = "c", long = "scenario-definitions")]
    ///Path to YAML file that defines the test configuration.
    ///
    ///Defaults to `tests_path/config/scenario_definitions.yaml`.
    scenario_definitions_path: String,

    #[arg(short = "t", long = "testsuites")]
    ///Select testsuites to run (default is to run all).
    testsuites: Vec<String>,

    #[arg(long = "pause-test")]
    ///Instruct test runner to pause when the given test is reached.
    pause_test: String,

    #[arg(long = "pause-event")]
    ///Instruct test runner to pause on a specific event.
    ///
    ///Possible values: none, failure, screen-mismatch, assert-screen-mismatch, command.
    pause_event: PauseEvent,

    #[arg(short = "v", long = "var")]
    ///Set a specific config variable.
    ///
    ///Can be specified multiple times.
    var: Vec<String>,

    #[arg(short = "k", long = "keep-container")]
    ///Keep the test container on exit. By default, the test container is deleted.
    keep_container: bool,
}

#[derive(Args, Debug)]
enum MySubCommand {
    // Run a specific testsuite.
    Run(CommandRun),
}

#[derive(Args, Debug)]
///Sam's OpenQA helper tool.
struct MyArgs {
    #[arg(short, long)]
    ///Enable verbose logging
    verbose: bool,
    #[arg(sub)]
    ///Select subcommand.
    ///
    ///Available subcommands: `run`
    cmd: MySubCommand,
}

/// Command to run the openQA test suite and wait until it completes.
impl CommandRun {
    /// Read scenario from local `scenario_definitions.yaml` config file.
    ///
    /// This is a standard openQA config file. See an example here:
    /// <https://github.com/os-autoinst/os-autoinst-distri-example/blob/main/scenario-definitions.yaml>
    fn get_scenarios_from_config(&self) -> Result<ScenarioDefinitions, anyhow::Error> {
        let tests_path = Path::new(&self.tests_path);
        let scenario_definitions_path: Box<Path> = if self.scenario_definitions_path.is_empty() {
            tests_path.join("config/scenario_definitions.yaml").into_boxed_path()
        } else {
            Path::new(&self.scenario_definitions_path).to_path_buf().into_boxed_path()
        };

        let scenarios = ScenarioDefinitions::load_from_file(&scenario_definitions_path)
            .with_context(|| format!(
                    "Failed to load scenario definitions from {}",
                    scenario_definitions_path.display())
            )?;
        Ok(scenarios)
    }

    /// Convert commandline args into openQA KEY=VALUE backend settings.
    fn get_test_config_vars(&self) -> Result<HashMap::<String, String>, anyhow::Error> {
        let mut extra_vars = HashMap::<String, String>::new();
        for var in &self.var {
            let (key, value) = var.split_once('=')
                .ok_or_else(|| anyhow!("Expected `KEY=VALUE` in `--var` argument, got: {}", var))?;
            extra_vars.insert(key.to_string(), value.to_string());
        }

        if !self.pause_test.is_empty() {
            extra_vars.insert("PAUSE_AT".to_string(), self.pause_test.to_string());
        };

        match self.pause_event {
            PauseEvent::None => None,
            PauseEvent::Failure => extra_vars.insert("PAUSE_ON_FAILURE".to_string(), "1".to_string()),
            PauseEvent::ScreenMismatch => extra_vars.insert("PAUSE_ON_SCREEN_MISMATCH".to_string(), "check_screen".to_string()),
            PauseEvent::AssertScreenMismatch => extra_vars.insert("PAUSE_ON_SCREEN_MISMATCH".to_string(), "assert_screen".to_string()),
            PauseEvent::Command => extra_vars.insert("PAUSE_ON_NEXT_COMMAND".to_string(), "1".to_string()),
        };

        Ok(extra_vars)
    }

    /// Determine what assets are specified in the test config.
    ///
    /// Return a VolumeSpec struct for each one.
    ///
    /// # Errors
    ///
    /// Returns an error if any specified host file does not exist.
    ///
    fn locate_assets(&self, config: &IsotovideoConfig) -> Result<Vec<VolumeSpec>, anyhow::Error> {
        const TEST_MEDIA_DIR: &str = "/";

        let mut volumes: Vec<VolumeSpec> = Vec::new();

        // 100 hard disks ought to be enough for anybody.
        const MAX_HDDS: usize = 100;

        for hdd_number in 1..MAX_HDDS {
            let key = format!("HDD_{hdd_number}");
            match config.get(&key) {
                Some(value) => {
                    let hdd_container_path = PathBuf::from(TEST_MEDIA_DIR)
                        .join(Path::new(value));
                    if hdd_number > self.hdd_path.len() {
                        bail!(
                            "Test {} requires {}={}. You must specify --hdd-path once for each configured disk to run this test",
                            config.test_name(),
                            key,
                            hdd_container_path.display(),
                        );
                    }

                    volumes.push(
                        VolumeSpec::from_paths(
                            Path::new(&self.hdd_path[hdd_number - 1]),
                            &hdd_container_path
                        )?
                    );
                },
                None => { break; },
            }
        }

        if config.contains_key("ISO") {
            let iso_container_path = PathBuf::from(TEST_MEDIA_DIR)
                .join(Path::new(config.get("ISO").unwrap()));

            if self.iso_path.is_empty() {
                bail!(
                    "Test {} requires ISO={}. You must specify --iso-path to run this test",
                    config.test_name(),
                    iso_container_path.display(),
                );
            }

            volumes.push(
                VolumeSpec::from_paths(
                    Path::new(&self.iso_path),
                    &iso_container_path
                )?
            );
        }

        let mut asset_id = 1;
        let mut asset_path_list = self.asset_path.clone();
        loop {
            let key = format!("ASSET_{asset_id}");
            if config.contains_key(&key) {
                let asset_path = asset_path_list.pop()
                    .context(
                        format!("This test sets {key}, you must specify the path to this asset with `--asset-path`.")
                    )?;
                let asset_path = Path::new(&asset_path);
                let dest_path = Path::new(&"/var/lib/openqa/share/factory/other")
                    .join(asset_path.file_name().unwrap());
                let volume = VolumeSpec::from_paths(asset_path, &dest_path)?;
                volumes.push(volume);
            } else {
                break;
            }
            asset_id += 1;
        }
        Ok(volumes)
    }

    /// Run a single testsuite.
    fn run_testsuite(
        &self,
        crun: &ContainerRuntime,
        frontend_event_rx: &mut mpsc::Receiver<FrontendEvent>,
        runner_event_tx: &mpsc::Sender<BackendEvent>,
        testsuite_name: &str,
        output: &OutputDirectory,
        isotovideo_config: IsotovideoConfig,
        user_config: UserConfig
    ) -> Result<TestStatus, anyhow::Error> {
        let output_subdir: PathBuf = output.create_subdir(testsuite_name)
            .with_context(|| format!("Failed to create output subdirectory for {testsuite_name}"))?;

        let assets: Vec<VolumeSpec> = self.locate_assets(&isotovideo_config)
            .with_context(|| format!("Failed to locate assets for {testsuite_name}"))?;

        let runner = Backend::new(
            crun,
            assets,
            PathBuf::from(&self.tests_path),
            isotovideo_config,
            PathBuf::from(&output_subdir),
            self.keep_container,
        )?;

        let test_status = runner.run(
            user_config,
            frontend_event_rx,
            |message| {
                runner_event_tx.send(message.clone())
                    .unwrap_or_else(|err| debug!("Backend event dropped:{}", err))
            },
        );
        debug!("Got test result: {:?}", test_status);

        match test_status {
            Ok(TestStatus::Abort) | Err(_) => {
                match output.remove_subdir(testsuite_name) {
                    Ok(path) => info!("Removed {}", path.display()),
                    Err(e) => warn!("{e}")
                };
            },
            _ => {},
        };
        test_status
    }

    /// Main entry point for `run` subcommand.
    fn execute(&self) -> Result<(), anyhow::Error> {
        let docker_path = env::var("DOCKER").ok();
        let crun = ContainerRuntime::new(docker_path.as_ref(), ProcessGroup::leader())?;

        let output = OutputDirectory::new(&self.output_path)?;
        let test_config_vars = self.get_test_config_vars()?;

        let scenarios = self.get_scenarios_from_config()?;
        let testsuites: Vec<String> = if self.testsuites.is_empty() {
            scenarios.get_testsuites()
        } else {
            self.testsuites.clone()
        };

        let user_config = UserConfig {
            pause_at_test: self.pause_test.clone(),
            pause_on_event: self.pause_event,
        };

        let (frontend_thread, mut frontend_event_rx, runner_event_tx) = frontends::start(&self.frontend, &crun, testsuites.clone(), output.clone(), user_config.clone());

        let mut overall_result: Result<TestStatus, anyhow::Error> = Ok(TestStatus::Pass);

        for testsuite_name in testsuites {
            let isotovideo_config = scenarios.get_isotovideo_config_for_testsuite(
                &testsuite_name,
                &test_config_vars,
            )?;

            let test_status = self.run_testsuite(&crun, &mut frontend_event_rx, &runner_event_tx, &testsuite_name, &output, isotovideo_config, user_config.clone());
            match test_status {
                Ok(TestStatus::Abort) => {
                    info!("Exiting main loop due to {test_status:?}");
                    overall_result = test_status;
                    break;
                },
                Ok(TestStatus::Pass) => {},
                Ok(TestStatus::Fail) => {
                    overall_result = Ok(TestStatus::Fail);
                },
                Err(ref e) => {
                    error!("Exiting main loop due to error in {testsuite_name}: {e:?}");
                    overall_result = test_status;
                    break;
                }
            }
        }

        let program_exit_event = match overall_result {
            Err(ref e) => BackendEvent::ProgramExit {
                testsuite_result: TestStatus::Abort,
                error: Some(e.to_string())
            },
            Ok(test_status) => BackendEvent::ProgramExit {
                testsuite_result: test_status,
                error: None
            },
        };

        info!("Sending ProgramExit to frontend");
        runner_event_tx.send(program_exit_event)
            .unwrap_or_else(
                |err| error!("Failed to send ProgramExit to frontend: {}", err)
            );

        info!("Waiting for frontend to stop");
        frontend_thread.join()
            .unwrap()
            .unwrap();

        match overall_result {
            Err(ref e) => Err(anyhow!("{}", e)),
            Ok(_) => Ok(()),
        }
    }
}

/// Main entry point.
fn main() -> ExitCode {
    let args = arg::parse_args::<MyArgs>();

    if args.verbose {
        env::set_var("RUST_LOG", "debug");
    }
    env_logger::init();

    match args.cmd {
        MySubCommand::Run(cmd) => {
            let result = cmd.execute();
            if let Err(e) = result {
                println!("{:#}", e);
                return ExitCode::from(1);
            }
        }
    }

    ExitCode::from(0)
}
