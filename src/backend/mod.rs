//! The test runner interface, which is effectively the backend of this tool.
//!
//! Use the [Backend] struct to run a specific openQA testsuite in a one-off
//! container.

mod event;

use anyhow::bail;
use log::*;

use std::path::{Path, PathBuf};
use std::sync::mpsc::{self, TryRecvError};
use std::thread;
use std::time::{Duration, Instant};

use crate::frontends::FrontendEvent;
use crate::helpers::container::{ContainerRuntime, VolumeSpec};
use crate::helpers::isotovideo::{
    self,
    IsotovideoConfig,
    IsotovideoHelper,
    IsotovideoPauseSettings,
    IsotovideoStatusMessage,
    OPENQA_VNC_PORT
};
use crate::types::{PauseEvent, TestStatus};
use crate::UserConfig;

pub use event::BackendEvent;

/* This insecure token would be a problem if the temporary isotovideo worker was exposed on a
 * hostile network. It's only available on localhost so risk of attack should be minimal. */
const INSECURE_JOB_TOKEN: &str = "abcdefgh";

/// How often the event loop wakes up to check events.
const TICK_INTERVAL_MS: u64 = 250; 

/// Test runner.
///
/// Runs the OpenQA test runner (osautoinst / isotovideo) inside a local
/// container, using [helpers::container].
pub struct Backend {
    // Helper classes
    crun: ContainerRuntime,

    // Test configuration
    run_id: String,
    volumes: Vec<VolumeSpec>,
    config: IsotovideoConfig,
    output_path: PathBuf,
    keep_container: bool,
}

struct RunState {
    user_config: UserConfig,
    awaiting_test_order: bool,
    awaiting_test_results: Vec<String>,
    current_test: String,
    paused: bool,
    finished: bool,
    result: TestStatus,
}

/// Get the host port for VNC remote desktop access to the system under test.
pub fn get_vnc_port(crun: &ContainerRuntime, container_name: &str) -> Result<u32, anyhow::Error> {
    let inspect_result = crun.inspect_container(container_name)?;
    let vnc_port = crun.get_host_tcp_port(&inspect_result, OPENQA_VNC_PORT)?;
    Ok(vnc_port)
}

fn setup_container_exit_handler(crun: &ContainerRuntime, container_name: &str, tx: mpsc::Sender::<BackendEvent>) {
    crun.spawn_watcher(container_name, move |result| {
        match result {
            Ok(exit_status) => {
                debug!("Container exited. Sending IsotovideoContainerExit event to main loop...");
                tx.send(BackendEvent::IsotovideoContainerExit(exit_status))
                    .unwrap_or_else(|err| debug!("Failed to send IsotovideoContainerExit to main loop: {}", err));
            },
            Err(err) => {
                error!("Error watching container. Sending InternalError event to main loop...");
                tx.send(BackendEvent::InternalError(err.to_string()))
                    .unwrap_or_else(|err| error!("Failed to send InternalError to main loop: {}", err));
            },
        }
    });
}

impl Backend {
    pub fn new(
            crun: &ContainerRuntime, 
            volumes: Vec<VolumeSpec>,
            tests_path: PathBuf,
            mut config: IsotovideoConfig,
            output_path: PathBuf,
            keep_container: bool
        ) -> Result<Self, anyhow::Error> {

        // The run ID is based on the output directory name.
        let run_id = output_path
            .file_name().expect("Invalid output path")
            .to_str().expect("Invalid UTF-8 in output path")
            .to_string();

        // The tests are mounted as a volume in the same way as the test media and assets.
        let tests_volume = VolumeSpec::from_paths(&tests_path, Path::new("/tests"))?;
        let mut volumes = volumes.clone();
        volumes.push(tests_volume);

        // The isotovideo workdir containing test results is shared with the host
        volumes.push(VolumeSpec::from_paths(&output_path, Path::new("/shared")).unwrap());

        // Test config which is defined by the test runner.
        config.add_key("ASSETDIR", "/var/lib/openqa/share/factory/")?;
        config.add_key("CASEDIR", "/tests")?;
        config.add_key("JOBTOKEN", INSECURE_JOB_TOKEN)?;
        config.add_key("NEEDLES_DIR", "/tests/needles")?;

        Ok(Self { crun: crun.clone(), run_id, volumes, config, output_path, keep_container })
    }

    fn container_name(&self) -> String {
        format!("ssam_openqa_{}", self.run_id)
    }

    /// Run the defined test suite and await completion.
    pub fn run<F>(&self, user_config: UserConfig, frontend_event_rx: &mut mpsc::Receiver<FrontendEvent>, event_callback: F) -> Result<TestStatus, anyhow::Error>
    where
        F: FnMut(&BackendEvent)
    {
        let container_name = self.container_name();
        let job_token = self.config.get("JOBTOKEN")
            .expect("JOBTOKEN is always set");

        isotovideo::start_isotovideo_container(&self.crun, &container_name, &self.volumes, &self.config)?;

        let mut log_follower = isotovideo::start_log_follower(&self.crun, &container_name, &self.output_path)?;

        let test_status = match self.main_loop(user_config, frontend_event_rx, event_callback, &self.crun, &container_name, job_token, &self.output_path) {
            Ok(test_status) => test_status,
            Err(err) => {
                eprintln!("Error running test: {}", err);
                TestStatus::Abort
            },
        };

        if test_status == TestStatus::Abort {
            self.stop_and_remove();
        } else {
            self.remove_container_maybe();
        }

        log_follower.kill()
            .unwrap_or_else(|err| error!("Failed to kill log follower process: {}", err));
        let log_follower_wait_result = log_follower.wait();
        if log_follower_wait_result.is_err() {
            error!("Failed to wait log follower process: {}", log_follower_wait_result.err().unwrap())
        };

        Ok(test_status)
    }

    /// This is the core event loop.
    ///
    /// First events from the test runner (`BackendEvent`) are processed, then control
    /// messages (`FrontendEvent`).
    fn main_loop<F>(
        &self,
        user_config: UserConfig,
        frontend_event_rx: &mut mpsc::Receiver<FrontendEvent>,
        mut send_to_frontend: F,
        crun: &ContainerRuntime,
        container_name: &str,
        job_token: &str,
        output_path: &Path
    ) -> Result<TestStatus, anyhow::Error>
    where
        F: FnMut(&BackendEvent)
    {
        let mut run_state = RunState {
            user_config,
            awaiting_test_order: true,
            awaiting_test_results: vec!(),
            current_test: "".to_string(),
            paused: false,
            finished: false,
            result: TestStatus::Pass,
        };

        let (tx, rx) = mpsc::channel::<BackendEvent>();

        let start_message = BackendEvent::IsotovideoContainerStart(container_name.to_string());
        send_to_frontend(&start_message);

        if run_state.user_config.pause_on_event == PauseEvent::Command {
            // If we set PAUSE_ON_NEXT_COMMAND=1 before the tests start,
            // Isotovideo will pause without sending any status messages.
            //
            // We create a pause event here so frontend shows the paused handler
            // and backend behaves correctly.
            run_state.paused = true;
            send_to_frontend(
                &BackendEvent::IsotovideoStatus(
                    IsotovideoStatusMessage::Paused { reason: Some("pause on next command".to_string()) }
                )
            );
        }

        setup_container_exit_handler(crun, container_name, tx.clone());

        let isotovideo = IsotovideoHelper::connect(crun, container_name, job_token)?;

        let mut tick = Instant::now();
        let mut next_tick = tick + Duration::from_millis(TICK_INTERVAL_MS);

        loop {
            trace!("Loop in backend");

            if run_state.awaiting_test_order {
                /* Test order is written to disk by isotovideo during early init */
                let path = isotovideo::test_order_path(output_path);
                match isotovideo::load_test_order(&path) {
                    Ok(test_order) => {
                        send_to_frontend(&BackendEvent::TestOrder(test_order));
                        run_state.awaiting_test_order = false;
                    },
                    Err(e) => {
                        trace!("No test order yet at {}: {}", path.display(), e);
                    }
                }
            }

            if !run_state.awaiting_test_results.is_empty() {
                /* Test results are written to disk by isotovideo when test finishes,
                 * but it may appear on the filesystem sometime later.
                 */
                let test_name = &run_state.awaiting_test_results[0];
                let result_path = isotovideo::test_result_path(output_path, test_name);
                if result_path.exists() {
                    let test_result = isotovideo::load_test_result(&result_path)?;
                    let test_status = test_result.status();
                    if test_status == TestStatus::Fail {
                        run_state.result = TestStatus::Fail;
                    }
                    send_to_frontend(&BackendEvent::TestResult { test_name: test_name.clone(), test_result });
                    run_state.awaiting_test_results.remove(0);
                }
            }

            let process_events_result = self.process_events(&rx, frontend_event_rx, &mut send_to_frontend, &isotovideo, &mut run_state);
            if let Err(e) = process_events_result {
                debug!("Error while processing events: {}", e);
                run_state.result = TestStatus::Abort;
                break;
            }
            if run_state.finished {
                break;
            }

            let now = Instant::now();
            if now < next_tick {
                let until_next_tick: Duration = next_tick - now;
                thread::sleep(until_next_tick);
            }

            tick = Instant::now();
            next_tick = tick + Duration::from_millis(TICK_INTERVAL_MS);
        };

        isotovideo.disconnect();
        Ok(run_state.result)
    }

    fn process_events<F>(
        &self,
        rx: &mpsc::Receiver<BackendEvent>,
        frontend_event_rx: &mpsc::Receiver<FrontendEvent>,
        send_to_frontend: &mut F,
        isotovideo: &IsotovideoHelper,
        run_state: &mut RunState,
    ) -> Result<(), anyhow::Error>
    where
        F: FnMut(&BackendEvent)
    {
        // First isotovideo status messages
        loop {
            trace!("Loop waiting for isotovideo status message");
            match isotovideo.try_recv_status_messages() {
                Ok(status) => {
                    let event = BackendEvent::IsotovideoStatus(status.clone());
                    send_to_frontend(&event);

                    match status {
                        IsotovideoStatusMessage::ConnectionFailed { error } => {
                            bail!("Connection error to isotovideo: {}", error);
                        }
                        IsotovideoStatusMessage::Paused { reason: _reason } => {
                            run_state.paused = true;
                        },
                        IsotovideoStatusMessage::ResumeTestExecution => {
                            run_state.paused = false;
                        }
                        IsotovideoStatusMessage::CurrentTest { name, full_name: _full_name } => {
                            let name = name.unwrap_or("".to_string());
                            if !run_state.current_test.is_empty() && name != run_state.current_test {
                                /* If isotovideo started a new test, the results of the
                                 * previous test will soon appear in the output directory.
                                 */
                                run_state.awaiting_test_results.push(run_state.current_test.clone());
                            }
                            run_state.current_test.clone_from(&name);
                        },
                        _ => {},
                    }
                },
                Err(TryRecvError::Empty) => break,
                Err(error) => return Err(anyhow::Error::new(error))
            }
        }

        // Now events from the container runtime.
        match rx.try_recv() {
            Ok(message) => {
                send_to_frontend(&message);

                match message {
                    BackendEvent::InternalError(error) => {
                        bail!(error);
                    },
                    BackendEvent::IsotovideoContainerExit(exit_status) => {
                        if run_state.awaiting_test_order {
                            send_to_frontend(&BackendEvent::TestsuiteBroken);
                        }

                        if exit_status != 0 {
                            run_state.result = TestStatus::Fail;
                        }

                        debug!("Returning testsuite result: {:?}", run_state.result);
                        send_to_frontend(&BackendEvent::TestsuiteResult { testsuite_result: run_state.result });

                        run_state.finished = true;
                        return Ok(())
                    }
                    _ => bail!("Unexpected message: {:?}", message),
                }
            },
            Err(TryRecvError::Empty) => {},
            Err(error) => return Err(anyhow::Error::new(error))
        }

        // Now all events from the frontend.
        loop {
            trace!("Loop waiting for frontend status message");
            match frontend_event_rx.try_recv() {
                Ok(message) => {
                    match message {
                        FrontendEvent::UserAbort => {
                            run_state.result = TestStatus::Abort;
                            run_state.finished = true;
                            return Ok(());
                        },
                        FrontendEvent::UserContinue => isotovideo.resume_test_execution(),
                        FrontendEvent::UserChangeConfig(user_config) => {
                            let old_settings = IsotovideoPauseSettings::new_from_pause_event_type(run_state.user_config.pause_on_event);
                            let new_settings = IsotovideoPauseSettings::new_from_pause_event_type(user_config.pause_on_event);

                            if old_settings.pause_on_failure != new_settings.pause_on_failure {
                                isotovideo.set_pause_on_failure(new_settings.pause_on_failure)?;
                            };
                            if old_settings.pause_on_next_command != new_settings.pause_on_next_command {
                                isotovideo.set_pause_on_next_command(new_settings.pause_on_next_command)?;
                            };
                            if old_settings.pause_on_screen_mismatch != new_settings.pause_on_screen_mismatch {
                                isotovideo.set_pause_on_screen_mismatch(&new_settings.pause_on_screen_mismatch)?;
                            };
                            if run_state.user_config.pause_at_test != user_config.pause_at_test {
                                isotovideo.set_pause_at_test(&user_config.pause_at_test)?;
                            }

                            run_state.user_config = user_config;
                        }
                        FrontendEvent::UserPause => {
                            if run_state.paused {
                                // This check isn't as useful as it looks.
                                // CTRL+C while reading from stdin in the pause handler
                                // will be silently ignored by the Rust standard library.
                                println!("Aborting test execution due to CTRL+C.");
                                run_state.result = TestStatus::Abort;
                                run_state.finished = true;
                                return Ok(());
                            }

                            println!("Pausing test execution due to CTRL+C.");
                            run_state.paused = true;

                            isotovideo.pause_test_execution();
                        }
                    }
                },
                Err(TryRecvError::Empty) => break,
                Err(error) => return Err(anyhow::Error::new(error))
            }
        }

        Ok(())
    }

    // Called on internal error to clean up the running container.
    fn stop_and_remove(&self) {
        let container_name = self.container_name();
        let stop_result = self.crun.stop_container(&container_name);
        if let Err(e) = stop_result {
            eprintln!("Error stopping container {container_name} after error: {e}");
        }
        self.remove_container_maybe();
    }

    fn remove_container_maybe(&self) {
        let container_name = self.container_name();
        if self.keep_container {
            println!("Test container kept as: {container_name}");
        } else {
            let remove_result = self.crun.remove_container(&container_name);
            if let Err(e) = remove_result {
                warn!("Error removing container {container_name}: {e}");
            };
        }
    }
}

