//! Events published by test runner.

use crate::helpers::isotovideo::{IsotovideoStatusMessage, IsotovideoTestResult};
use crate::TestStatus;

use serde::Serialize;

/// Events sent by runner main loop.
#[derive(Clone, Debug, Serialize)]
pub enum BackendEvent {
    /// Send when the main loop exits
    ProgramExit { testsuite_result: TestStatus, error: Option<String> },
    /// Sent when an error occurs in a helper thread.
    InternalError(String),
    /// Sent when the runner main loop starts a container for the given testsuite.
    IsotovideoContainerStart(String),
    /// Sent when container exit is detected.
    IsotovideoContainerExit(u64),
    /// Sent on isotovideo websocket events.
    IsotovideoStatus(IsotovideoStatusMessage),
    /// Sent when the test order for the current testsuite is available.
    TestOrder(Vec<String>),
    /// Sent when results for the current test are available.
    TestResult { test_name: String, test_result: IsotovideoTestResult },
    /// Sent when the testsuite finishes
    TestsuiteResult { testsuite_result: TestStatus },
    /// Sent when the isotovideo process exits before the test order is generated.
    TestsuiteBroken,
}
