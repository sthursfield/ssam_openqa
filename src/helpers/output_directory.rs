use std::fs;
use std::path::{Path, PathBuf};

use anyhow::{anyhow, bail};

/// Helper to create and manage the directory where test data is written.
#[derive(Clone)]
pub struct OutputDirectory {
    base_path: PathBuf,
}

impl OutputDirectory {
    /// Prepares the OutputDirectory struct.
    ///
    /// Fails if the output directory already exists. The directory isn't
    /// created until we manage to start the test though to avoid cluttering
    /// the filesystem.
    pub fn new(base_dir: &str) -> Result<Self, anyhow::Error> {
        let base_path = Path::new(base_dir);
        if base_path.exists() {
            bail!(
                "Output directory {} already exists. Please remove it.",
                base_path.display()
            );
        };
        Ok(Self { base_path: PathBuf::from(base_path) })
    }

    /// Create a named subdir of the output directory and return a PathBuf.
    ///
    /// Fails if the subdirectory already exists.
    pub fn create_subdir(&self, name: &str) -> Result<PathBuf, anyhow::Error> {
        let path = self.base_path.as_path().join(name);

        // This should never happen as we already checked if the output dir exists, but to
        // avoid parallel invocations interfering with each other, let's check again.
        if path.exists() {
            bail!(
                "Output directory {} already exists. Please remove it.",
                path.display()
            );
        };

        fs::create_dir_all(&path)?;
        Ok(path)
    }

    pub fn remove_subdir(&self, name: &str) -> Result<PathBuf, anyhow::Error> {
        let path = self.base_path.as_path().join(name);
        match fs::remove_dir(&path) {
            Ok(_) => Ok(path),
            Err(e) => Err(anyhow!("Couldn't remove {}: {e}", path.display())),
        }
    }

    /// Get named subdir of the output directory and return a PathBuf.
    ///
    /// Fails if the subdirectory does not exists.
    pub fn get_subdir(&self, name: &str) -> Result<PathBuf, anyhow::Error> {
        let path = self.base_path.as_path().join(name);

        // This should never happen as we already checked if the output dir exists, but to
        // avoid parallel invocations interfering with each other, let's check again.
        if path.exists() {
            Ok(path)
        } else {
            Err(anyhow!("Directory {} does not exist in output dir", name))
        }
    }

    /// Get the path to isotovideo stderr output.
    pub fn get_isotovideo_log_for_testsuite(&self, testsuite_name: &str) -> Result<PathBuf, anyhow::Error> {
        let mut subdir = self.get_subdir(testsuite_name)?;
        subdir.push("_isotovideo.stderr.log");
        Ok(subdir)
    }

    /// Get input FIFO for a virtio console.
    pub fn get_virtio_console_input(&self, testsuite_name: &str, terminal_name: &str) -> Result<PathBuf, anyhow::Error> {
        let mut subdir = self.get_subdir(testsuite_name)?;
        subdir.push(format!("{terminal_name}.in"));
        Ok(subdir)
    }

    /// Get output FIFO for a virtio console.
    pub fn get_virtio_console_output(&self, testsuite_name: &str, terminal_name: &str) -> Result<PathBuf, anyhow::Error> {
        let mut subdir = self.get_subdir(testsuite_name)?;
        subdir.push(format!("{terminal_name}.out"));
        Ok(subdir)
    }
}

