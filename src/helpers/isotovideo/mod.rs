//! Helpers for isotovideo, the openQA test runner.

pub mod config;
pub mod container;
pub mod control;
pub mod control_message;
pub mod log_follower;
pub mod log_parser;
pub mod pause_settings;
pub mod status_message;
pub mod test_order;
pub mod test_result;

use anyhow::{self, bail};
use log::*;

use std::sync::mpsc::{self, TryRecvError};

use crate::helpers::container::ContainerRuntime;

pub use self::config::IsotovideoConfig;
pub use self::container::start_isotovideo_container;
use self::control::watch_websocket_in_thread;
use self::control_message::IsotovideoControlMessage;
pub use self::log_follower::start_log_follower;
pub use self::log_parser::get_isotovideo_log_highlights;
pub use self::pause_settings::IsotovideoPauseSettings;
pub use self::status_message::IsotovideoStatusMessage;
pub use self::test_order::{load_test_order, test_order_path};
pub use self::test_result::{IsotovideoTestResult, load_test_result, test_result_path};

pub const OPENQA_WORKER_PORT: u32 = 20013;
pub const OPENQA_VNC_PORT: u32 = 5990;

/// Top level helper class for communicating with a running Isotovideo process
/// using the control websocket.
pub struct IsotovideoHelper {
    // This uses the tokio async channel type, because its communicating with
    // an async_tungstenite websocket. Tungstenite websockets can only be
    // used from async code.
    control_tx: tokio::sync::mpsc::UnboundedSender::<IsotovideoControlMessage>,

    status_tx: mpsc::Sender::<IsotovideoStatusMessage>,
    status_rx: mpsc::Receiver::<IsotovideoStatusMessage>,
}

impl IsotovideoHelper {
    /// Create a new instance of the helper for a running container.
    ///
    /// This function will block until able to connect with the
    /// control websocket.
    ///
    /// This function spawns a thread which will run until the
    /// [IsotovideoHelper::disconnect()] method is called
    ///
    /// The `event_callback` will be called from the thread every time
    /// a message is received from Isotovideo.
    pub fn connect(
        crun: &ContainerRuntime,
        container_name: &str,
        job_token: &str,
    ) -> Result<Self, anyhow::Error>
    {
        let inspect_result = crun.inspect_container(container_name)?;
        let worker_command_port = crun.get_host_tcp_port(&inspect_result, OPENQA_WORKER_PORT)?;

        let (status_tx, status_rx) = mpsc::channel();

        let status_tx_clone = status_tx.clone();
        let control_tx = watch_websocket_in_thread(worker_command_port, job_token, move |status_message| {
            status_tx_clone.send(status_message.clone())
                // Error indicates that the channel closed because the process is in shutdown.
                .unwrap_or_else(|err|
                    debug!("Failed to transfer message {:?} to isotovideo helper status channel: {}", status_message, err)
                );
        })?;

        Ok(Self { control_tx, status_tx, status_rx })
    }

    pub fn disconnect(&self) {
        self.control_tx.send(IsotovideoControlMessage::DisconnectWatcher)
            // This can happen if there's an error with the websocket connection.
            .unwrap_or_else(|_e| debug!("isotovideo control channel already closed"));
    }

    /// Receive status messages from Isotovideo, if available.
    ///
    /// See mpsc::Receiver::try_recv() for details on how this works.
    pub fn try_recv_status_messages(&self) -> Result<IsotovideoStatusMessage, TryRecvError> {
        self.status_rx.try_recv()
    }

    /// Pause test execution if tests are running.
    pub fn pause_test_execution(&self) {
        self.send(IsotovideoControlMessage::PauseTestExecution);
    }

    /// Resume test execution if tests are currently paused.
    pub fn resume_test_execution(&self) {
        self.send(IsotovideoControlMessage::ResumeTestExecution);
    }

    /// Set "pause_at_test" setting.
    pub fn set_pause_at_test(&self, name: &str) -> Result<(), anyhow::Error> {
        self.send_and_await_ok(IsotovideoControlMessage::SetPauseAtTest { name: name.to_string() })
    }

    /// Enable or disable the "pause_on_failure" setting.
    pub fn set_pause_on_failure(&self, flag: bool) -> Result<(), anyhow::Error> {
        self.send_and_await_ok(IsotovideoControlMessage::SetPauseOnFailure { flag })
    }

    /// Enable or disable the "pause_on_next_command" setting.
    pub fn set_pause_on_next_command(&self, flag: bool) -> Result<(), anyhow::Error> {
        self.send_and_await_ok(IsotovideoControlMessage::SetPauseOnNextCommand { flag })
    }

    /// Set the value of the "pause_on_screen_mismatch" setting.
    ///
    /// Valid values are: empty string, "check_screen" and "assert_screen".
    pub fn set_pause_on_screen_mismatch(&self, pause_on: &str) -> Result<(), anyhow::Error> {
        self.send_and_await_ok(IsotovideoControlMessage::SetPauseOnScreenMismatch { pause_on: pause_on.to_string() })
    }

    // Send control message to the Isotovideo process.
    //
    // If the control message cannot be sent for any reason, an error
    // is logged. This usually indicates the Isotovideo process has
    // stopped running.
    fn send(&self, message: IsotovideoControlMessage) {
        let message_name: String = message.name().to_string();
        debug!("Sending control message to isotovideo: {}", message_name);
        self.control_tx.send(message)
            .unwrap_or_else(|err|
                error!("Failed to send message {} to isotovideo control channel: {}", message_name, err)
            );
    }

    // Send control message to Isotovideo and block until the process receives "OK" response.
    //
    // If the response is not "ok" then an error is returned.
    //
    // If no "ret" message is received this will block forever.
    fn send_and_await_ok(&self, control_message: IsotovideoControlMessage) -> Result<(), anyhow::Error> {
        let control_message_name = control_message.name().to_string();

        // First empty all the status messages in the queue, just in case there are any stale
        // Response messages.
        let mut status_messages: Vec<IsotovideoStatusMessage> = self.status_rx.try_iter().collect();

        // Now send the message.
        self.send(control_message);

        // Now wait til we get the "ok".
        loop {
            trace!("Loop waiting for isotovideo response");
            match self.status_rx.recv() {
                Ok(message) => {
                    match message {
                        IsotovideoStatusMessage::Response { ret } => {
                            if ret {
                                // Everything is OK, go to post any status messages that were held
                                // back.
                                break;
                            } else {
                                bail!("Isotovideo sent failure response for message: {}", control_message_name);
                            }
                        },
                        _ => status_messages.push(message),
                    }
                }
                Err(error) => return Err(anyhow::Error::new(error))
            }
        };

        for status_message in status_messages {
            self.status_tx.send(status_message)
                .unwrap_or_else(|err|
                    error!("Failed to transfer message to isotovideo helper status channel: {}", err)
                );
        }

        Ok(())
    }
}
