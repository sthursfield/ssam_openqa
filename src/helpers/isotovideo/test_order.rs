use std::fs;
use std::path::{PathBuf, Path};

use anyhow::{self, bail};

type JsonValue = serde_json::value::Value;

fn load_test_order_entry(value: &JsonValue) -> Result<String, anyhow::Error> {
    match value {
        JsonValue::Object(object) => {
            match object.get("name") {
                Some(value) => {
                    match value {
                        JsonValue::String(name) => Ok(name.as_str().to_string()),
                        _ => bail!("Expected string for 'name' field"),
                    }
                },
                None => bail!("Missing 'name' field"),
            }
        },
        _ => bail!("Expected JSON object"),
    }
}

pub fn test_order_path(output_path: &Path) -> PathBuf {
    let mut path = PathBuf::from(output_path);
    path.push("testresults");
    path.push("test_order.json");
    path
}

pub fn load_test_order(path: &Path) -> Result<Vec<String>, anyhow::Error> {
    let text = fs::read_to_string(path)?;

    match serde_json::from_str(&text) {
        Ok(JsonValue::Array(array)) => {
            let result_vec = array.iter().map(load_test_order_entry);
            result_vec.collect()
        },
        Ok(_) => bail!("Expected JSON array."),
        Err(e) => bail!("Failed to parse test order as JSON: {}", e),
    }
}
