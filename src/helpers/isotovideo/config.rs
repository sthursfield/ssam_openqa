//! Defines configuration for a specific `isotovideo` invocation.

use std::collections::HashMap;
use std::clone::Clone;

use anyhow::{self, bail};

#[derive(Clone)]
pub struct IsotovideoConfig {
    vars: HashMap<String, String>,
}


impl IsotovideoConfig {
    pub fn new() -> Self {
        Self {
            vars: HashMap::new()
        }
    }

    /// Set the given key, or return an error if it has already been set.
    pub fn add_key(&mut self, key: &str, value: &str) -> Result<(), anyhow::Error> {
        if self.vars.contains_key(key) {
            bail!("Key {key} already set in isotovideo config")
        };
        self.vars.insert(key.to_string(), value.to_string());
        Ok(())
    }

    /// Add/update the config from `vars`, overwriting any existing values.
    pub fn update(&mut self, extra_vars: &HashMap::<String, String>) {
        //self.vars.extend(extra_vars);
        for (key, value) in extra_vars.iter() {
            self.vars.insert(key.to_string(), value.to_string());
        }
    }

    /// Return all key/value pairs in a stable order (alphabetically sorted).
    pub fn values_sorted(&self) -> Vec<(&String, &String)> {
        let mut keys: Vec<&String> = self.vars.keys().collect();

        keys.sort();

        let mut values = Vec::new();
        for key in keys {
            let value = self.vars.get(key)
                .expect("Key should have value");

            values.push((key, value));
        };
        values
    }

    pub fn contains_key(&self, k: &str) -> bool {
        self.vars.contains_key(k)
    }

    pub fn get(&self, k: &str) -> Option<&String> {
        self.vars.get(k)
    }

    pub fn test_name(&self) -> String {
        self.vars.get("TEST")
            .unwrap_or(&"<no name>".to_string())
            .clone()
    }
}
