use process_wrap::std::*;

use std::fs::File;
use std::path::{Path, PathBuf};

use crate::helpers::container::{ContainerRuntime};

/// Starts a subprocess that saves isotovideo logs to disk.
pub fn start_log_follower(crun: &ContainerRuntime, container_name: &str, output_path: &Path) -> Result<Box<dyn StdChildWrapper>, anyhow::Error> {
    let mut log_stderr_path = PathBuf::from(output_path);
    log_stderr_path.push("_isotovideo.stderr.log");
    let mut log_stdout_path = PathBuf::from(output_path);
    log_stdout_path.push("_isotovideo.stdout.log");

    let log_stderr = File::create(log_stderr_path).expect("failed to open container log file");
    let log_stdout = File::create(log_stdout_path).expect("failed to open container log file");

    let mut cmd_wrap = crun.command();
    let cmd = cmd_wrap.command_mut();
    cmd.arg("logs");
    cmd.arg("--follow");
    cmd.arg(container_name);
    cmd.stderr(log_stderr);
    cmd.stdout(log_stdout);

    let child = cmd_wrap.spawn()?;
    Ok(child)
}
