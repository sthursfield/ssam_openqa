//! Isotovideo control commands.



type JsonValue = serde_json::value::Value;
type JsonObject = serde_json::Map<String, JsonValue>;

/// Isotovideo control commands.
///
/// These are defined here:
/// <https://github.com/os-autoinst/os-autoinst/blob/master/OpenQA/Isotovideo/CommandHandler.pm>
///
/// Each `_handle_command_xxx` function defines a command `xxx`. This enum lists only the subet
/// used in ssam_openqa.
pub enum IsotovideoControlMessage {
    PauseTestExecution,
    ResumeTestExecution,
    SetPauseAtTest { name: String },
    SetPauseOnFailure { flag: bool },
    SetPauseOnNextCommand { flag: bool },
    SetPauseOnScreenMismatch { pause_on: String },
    // This is a special message to the websocket watch thread
    // to shut down.
    DisconnectWatcher,
}

impl IsotovideoControlMessage {
    pub fn name(&self) -> &str {
        match self {
            IsotovideoControlMessage::PauseTestExecution => "pause_test_execution",
            IsotovideoControlMessage::ResumeTestExecution => "resume_test_execution",
            IsotovideoControlMessage::SetPauseAtTest { name: _ } => "set_pause_at_test",
            IsotovideoControlMessage::SetPauseOnFailure { flag: _ } => "set_pause_on_failure",
            IsotovideoControlMessage::SetPauseOnNextCommand { flag: _ } => "set_pause_on_next_command",
            IsotovideoControlMessage::SetPauseOnScreenMismatch { pause_on: _ } => "set_pause_on_screen_mismatch",
            IsotovideoControlMessage::DisconnectWatcher => "[disconnect watcher thread]",
        }
    }

    pub fn to_json(&self) -> String {
        let mut map = JsonObject::new();

        map.insert("cmd".to_string(), JsonValue::String(self.name().to_string()));
        match self {
            IsotovideoControlMessage::SetPauseAtTest { name } => {
                map.insert("name".to_string(), JsonValue::String(name.clone()))
                    .unwrap();
            },
            IsotovideoControlMessage::SetPauseOnFailure { flag } => {
                map.insert("flag".to_string(), JsonValue::Bool(*flag))
                    .unwrap();
            },
            IsotovideoControlMessage::SetPauseOnNextCommand { flag } => {
                map.insert("flag".to_string(), JsonValue::Bool(*flag))
                    .unwrap();
            },
            IsotovideoControlMessage::SetPauseOnScreenMismatch { pause_on } => {
                map.insert("pause_on".to_string(), JsonValue::String(pause_on.to_string()))
                    .unwrap();
            },
            _ => {},
        }
        JsonValue::Object(map.clone()).to_string()
    }
}
