//! Handles communication with a running Isotovideo process.

use anyhow::{anyhow, bail, Context};
use log::*;
use futures::stream::StreamExt;
use tokio::runtime;
use tokio::sync::mpsc;

use std::env;
use std::thread;
use std::time::{Duration, Instant};

use super::control_message::IsotovideoControlMessage;
use super::status_message::IsotovideoStatusMessage;

const ISOTOVIDEO_READY_TIMEOUT_SECONDS: u64 = 10;

type JsonValue = serde_json::value::Value;

struct IsotovideoControl {
    port: u32,
    job_token: String,
}

impl IsotovideoControl {
    fn new(port: u32, job_token: String) -> Self {
        IsotovideoControl { port, job_token }
    }

    fn url(&self, endpoint: &str) -> String {
        format!("http://localhost:{}/{}/{}", self.port, self.job_token, endpoint)
    }

    fn websocket_url(&self) -> String {
        match env::var("_SSAM_OPENQA_WEBSOCKET_URL") {
            Ok(url) => {
                // This is used for fault injection during integration tests.
                debug!("Injecting custom websocket URL {}", url);
                url
            },
            Err(env::VarError::NotPresent) => {
                // This is the correct URL to use for the websocket.
                format!("ws://localhost:{}/{}/ws", self.port, self.job_token)
            },
            Err(env::VarError::NotUnicode(_)) => panic!("Invalid UTF-8 in _SSAM_OPENQA_WEBSOCKET_URL env var")
        }
    }

    // Get the version reported by isotovideo.
    //
    // This uses synchronous IO and will block until the endpoint responds
    // or a request timeout or other error occurs.
    fn get_isotovideo_version(&self) -> Result<u64, anyhow::Error> {
        let url = self.url("isotovideo/version");

        info!("Querying {url}");
        let response = minreq::get(url)
            .send()?;
        let response_text = response.as_str()?;

        parse_isotovideo_version_response(response_text)
            .context("Failed to parse isotovideo version response")
    }

    // Block the thread until the isotovideo control HTTP endpoint is responsive.
    fn await_ready(&self) -> Result<(), anyhow::Error> {
        let start_time = Instant::now();
        loop {
            trace!("Loop waiting for isotovideo endpoint");

            // Start by waiting a whole second - isotovideo is unlikely to respond
            // faster than that, as of 2023-06-04.
            std::thread::sleep(Duration::from_secs(1));

            if let Ok(version) = self.get_isotovideo_version() {
                info!("isotovideo HTTP endpoint version {} is ready", version);
                break;
            }

            let elapsed_secs = (Instant::now() - start_time).as_secs();
            info!("isotovideo HTTP endpoint is not ready after {} seconds", elapsed_secs);

            if elapsed_secs > ISOTOVIDEO_READY_TIMEOUT_SECONDS {
                bail!(
                    "isotovideo didn't respond over HTTP after {} seconds",
                    elapsed_secs
                );
            }
        }
        Ok(())
    }
}

/// Open control endpoint to running Isotovideo process.
///
/// Status messages are passed to the `status_message_callback` function.
///
/// Control messages can be sent using the returned [mpsc::Sender] channel. The caller MUST send
/// message [IsotovideoControlMessage::DisconnectWatcher] before closing the channel.
///
/// This function blocks until websocket is ready to send and receive messages.
pub fn watch_websocket_in_thread<F>(port: u32, job_token: &str, mut status_message_callback: F) -> Result<mpsc::UnboundedSender::<IsotovideoControlMessage>, anyhow::Error>
where
    F: FnMut(IsotovideoStatusMessage) + Send + 'static {

    let control = IsotovideoControl::new(port, job_token.to_string());
 
    // Following the docs at <https://docs.rs/tokio/1.37.0/tokio/sync/mpsc/index.html>:
    //
    // > For sending a message from sync to async, you should use an unbounded Tokio mpsc channel.
    //
    let (control_tx, mut control_rx) = mpsc::unbounded_channel::<IsotovideoControlMessage>();

    thread::spawn(move || {
        let runtime = runtime::Builder::new_current_thread()
            .enable_io()
            .build()
            .unwrap();

        runtime.block_on(async move {
            // This will block the runtime thread.
            let ready_result = control.await_ready();
            if let Err(err) = ready_result {
                status_message_callback(
                    IsotovideoStatusMessage::ConnectionFailed {
                        error: format!("await_ready failed: {err}").to_string()
                    }
                );
                return;
            };

            let url = control.websocket_url();
            let connect_result = async_tungstenite::tokio::connect_async(&url)
                .await;
            if let Err(err) = connect_result {
                status_message_callback(
                    IsotovideoStatusMessage::ConnectionFailed {
                        error: format!("connect_async({url}) failed: {err}").to_string()
                    }
                );
                return;
            };
            let (mut websocket, _response) = connect_result.unwrap();

            loop {
                trace!("Loop waiting for isotovideo control message");
                tokio::select!{
                    websocket_message = websocket.next() => {
                        let websocket_message = match websocket_message {
                            Some(websocket_message) => websocket_message,
                            None => {
                                status_message_callback(IsotovideoStatusMessage::ConnectionFailed {
                                    error: "Websocket closed unexpectedly".to_string()
                                });
                                break;
                            }
                        };

                        match websocket_message {
                            Ok(websocket_message) => {
                                match IsotovideoStatusMessage::from_websocket_message(&websocket_message) {
                                    Ok(isotovideo_message) => {
                                        let should_exit = matches!{
                                            isotovideo_message,
                                            IsotovideoStatusMessage::StopProcessingIsotovideoCommands { stop: _stop }
                                        };

                                        status_message_callback(isotovideo_message);

                                        if should_exit {
                                            break;
                                        }
                                    }
                                    Err(e) => {
                                        eprintln!("Unhandled isotovideo status message: {}", e);
                                    }
                                }
                            },
                            Err(e) => {
                                error!("Unable to read isotovideo status message: {}", e);
                            },
                        }
                    },
                    control_message_option = control_rx.recv() => {
                        let control_message = control_message_option
                            .expect("Control websocket snould not close until DisconnectWatcher message is sent.");
                        if let IsotovideoControlMessage::DisconnectWatcher = control_message {
                            break;
                        }
                        let message = tungstenite::Message::text(control_message.to_json());
                        websocket.send(message)
                            .await
                            .unwrap_or_else(|err| error!("Failed to send message to isotovideo control socket: {}", err));
                    }
                }
            };
            debug!("Exiting isotovideo websocket thread");
        });
    });

    Ok(control_tx)
}


fn parse_isotovideo_version_response(text: &str) -> Result<u64, anyhow::Error> {
    match serde_json::from_str(text) {
        Ok(JsonValue::Object(version_info)) => {
            match version_info.get("version") {
                Some(JsonValue::Number(version)) => {
                    let version_u64 = version.as_u64()
                        .ok_or(anyhow!("version number couldn't be converted to u64"))?;
                    Ok(version_u64)
                },
                Some(value) => bail!("unexpected value for `version`: {value:?}"),
                None => bail!("missing `version` field"),
            }
        },
        Ok(value) => bail!("unexpected value for version info: {value:?}"),
        Err(e) => bail!("failed to parse as JSON: {e}"),
    }
}
