//! Pause settings in the format isotovideo expects.

use crate::types::PauseEvent;

/// Pause settings in the format expected by Isotovideo.
pub struct IsotovideoPauseSettings {
    pub pause_on_failure: bool,
    pub pause_on_next_command: bool,
    pub pause_on_screen_mismatch: String,
}

impl IsotovideoPauseSettings {
    /// In ssam_openqa we have one enum to represent all types of pause events.
    /// In Isotovideo there are 3 separate settings. This function translates
    /// between the two.
    pub fn new_from_pause_event_type(pause_event: PauseEvent) -> Self {
        let mut pause_on_screen_mismatch: &str = "";
        let mut pause_on_next_command: bool = false;
        let mut pause_on_failure: bool = false;

        match pause_event {
            PauseEvent::None => {},
            PauseEvent::Failure => { pause_on_failure = true },
            PauseEvent::ScreenMismatch => { pause_on_screen_mismatch = "check_screen" },
            PauseEvent::AssertScreenMismatch => { pause_on_screen_mismatch = "assert_screen" },
            PauseEvent::Command => { pause_on_next_command = true },
        };

        Self {
            pause_on_failure,
            pause_on_next_command,
            pause_on_screen_mismatch: pause_on_screen_mismatch.to_string(),
        }
    }
}
