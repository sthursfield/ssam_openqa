//! Helpers for parsing Isotovideo log messages.

use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::str::FromStr;

use anyhow::anyhow;
use log::*;

#[derive(Clone, Copy, PartialEq)]
pub enum IsotovideoLogLevel {
    Error,
    Warn,
    Info,
    Debug,
}

impl FromStr for IsotovideoLogLevel {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "[error]" => Ok(IsotovideoLogLevel::Error),
            "[warn]" => Ok(IsotovideoLogLevel::Warn),
            "[info]" => Ok(IsotovideoLogLevel::Info),
            "[debug]" => Ok(IsotovideoLogLevel::Debug),
            _ => Err(anyhow!(format!("Unknown isotovideo log level: {}", s))),
        }
    }
}

pub struct IsotovideoLogMessage {
    pub level: IsotovideoLogLevel,
    pub text: String,
}

impl IsotovideoLogMessage {
    pub fn new(level: IsotovideoLogLevel, text: String) -> Self {
        Self { level, text }
    }
}

pub fn get_isotovideo_log_highlights(isotovideo_stderr: &Path) -> Result<Vec<IsotovideoLogMessage>, anyhow::Error> {
    info!("Parsing isotovideo log: {}", isotovideo_stderr.display());

    let mut result: Vec<IsotovideoLogMessage> = vec!();

    let mut log_text: String = "".to_string();
    File::open(isotovideo_stderr)?
        .read_to_string(&mut log_text)?;
    for line in log_text.lines() {
        let parts: Vec<&str> = line.splitn(3, ' ').collect();
        if parts.len() < 3 {
            debug!("Couldn't parse log line: {}", line);
        } else {
            let _date = parts[0];
            let log_level_str = parts[1];
            let text = parts[2];

            let log_level_parse_result = IsotovideoLogLevel::from_str(log_level_str);
            if let Ok(log_level) = log_level_parse_result {
                match log_level {
                    IsotovideoLogLevel::Warn | IsotovideoLogLevel::Error => {
                        result.push(IsotovideoLogMessage::new(log_level, text.to_string()));
                    },
                    IsotovideoLogLevel::Info => {
                        // Info messages aren't interesting, except the one about test failure.
                        if text.contains("# Test died:") {
                            result.push(IsotovideoLogMessage::new(log_level, text.to_string()));
                        }
                    },
                    _ => {},
                };
            } else {
                debug!("Couldn't parse log level: {}", log_level_str);
            }
        }
    };
    Ok(result)
}

#[cfg(test)]
mod tests {
    use std::env;
    use std::path::PathBuf;

    use super::*;

    #[test]
    fn test_example() {
        let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

        let mut example_log_path = PathBuf::from(manifest_dir);
        example_log_path.push("src/helpers/isotovideo/testdata/_isotovideo.stderr.log");

        let result = get_isotovideo_log_highlights(example_log_path.as_path())
            .unwrap();
        for log_message in result.iter() {
            println!("{}", log_message.text);
        }
        assert_eq!(result.len(), 4);
    }
}
