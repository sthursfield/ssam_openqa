use anyhow::bail;
use log::*;
use process_wrap::std::StdCommandWrap;

use std::process::Stdio;

use crate::helpers::container::{ContainerRuntime, VolumeSpec};
use crate::helpers::isotovideo::{IsotovideoConfig, OPENQA_WORKER_PORT, OPENQA_VNC_PORT};

const OPENQA_WORKER_IMAGE: &str = "registry.opensuse.org/devel/openqa/containers15.6/openqa_worker:latest";

fn build_container_run_command(crun: &ContainerRuntime, container_name: &str, volumes: &[VolumeSpec], config: &IsotovideoConfig) -> StdCommandWrap {
    let mut command_wrap = crun.command();

    let command = command_wrap.command_mut();
    command.arg("run");
    command.args(["--name", container_name]);
    command.arg("--privileged");
    command.arg("--detach");
    for volume in volumes.iter() {
        command.arg(volume.as_container_cli_argument());
    }
    command.args(["--entrypoint", "isotovideo"]);
    command.args(["--publish", &OPENQA_VNC_PORT.to_string()]);
    command.args(["--publish", &OPENQA_WORKER_PORT.to_string()]);
    command.arg(OPENQA_WORKER_IMAGE);

    command.arg("--workdir=/shared");
    for (key, value) in config.values_sorted() {
        command.arg(format!("{}={}", key, value));
    }

    command.stdout(Stdio::piped());
    command.stderr(Stdio::piped());

    command_wrap
}

pub fn start_isotovideo_container(
    crun: &ContainerRuntime,
    container_name: &str,
    volumes: &[VolumeSpec],
    config: &IsotovideoConfig
) -> Result<(), anyhow::Error> {
    if crun.container_exists(container_name)? {
        bail!("Container named {container_name} already exists.");
    }

    let mut command_wrap = build_container_run_command(
        crun,
        container_name,
        volumes,
        config
    );

    debug!("Running: {command_wrap:?}");
    let child = command_wrap.spawn()?;
    let output = child.wait_with_output()?;

    if output.status.success() {
        Ok(())
    } else {
        let process = command_wrap.command().get_program().to_string_lossy();
        let exit_code = output.status.code().unwrap();
        let stderr_text = if !output.stderr.is_empty() {
            format!("Error: {}", String::from_utf8_lossy(&output.stderr))
        } else {
            "Error: [none]".to_string()
        };

        bail!("Failed to start container: {process} returned code {exit_code}.\n{stderr_text}");
    }
}
