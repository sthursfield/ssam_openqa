//! Status messages that Isotovideo sends to command servers.
//!
//! Status messages are sent here:
//! <https://github.com/os-autoinst/os-autoinst/blob/master/OpenQA/Isotovideo/CommandHandler.pm>
//!
//! You can see the list of messages that the web UI handles in the `messageToStatusVariable`
//! object here:
//! <https://github.com/os-autoinst/openQA/blob/master/assets/javascripts/running.js>.

use anyhow::{anyhow, bail};
use log::*;
use serde::Serialize;
use tungstenite::Message;

type JsonValue = serde_json::value::Value;
type JsonObject = serde_json::Map<String, JsonValue>;

#[derive(Clone, Debug, Serialize)]
pub enum IsotovideoStatusMessage {
    /// Internal signal for when the websocket connection fails.
    ConnectionFailed { error: String },
    /// Internal marker for messages we don't care about.
    Ignored,
    /// Messages sent by Isotovideo over the websocket.
    CurrentApiFunction { function: String, /* details: json::Object, */ },
    CurrentTest { name: Option<String>, full_name: Option<String> },
    Paused { reason: Option<String> },
    ResumeTestExecution,
    StopProcessingIsotovideoCommands { stop: bool },
    StoppingTestExecution { reason: String },
    // Generic response to config queries and updates, implemented as
    // _response_ok() in OpenQA::Isotovideo::CommandHandler.
    Response { ret: bool },
}

impl IsotovideoStatusMessage {
    fn parse_current_api_function(object: &JsonObject) -> Result<Self, anyhow::Error> {
        let function = object.get("current_api_function").unwrap()
            .as_str()
            .ok_or(anyhow!("Missing current_api_function field"))?;
        Ok(IsotovideoStatusMessage::CurrentApiFunction { function: function.to_string() })
    }

    fn parse_current_test(object: &JsonObject) -> Result<Self, anyhow::Error> {
        let name: Option<String> = object.get("set_current_test")
            .and_then(|v| v.as_str()).map(|s| s.to_string());
        let full_name: Option<String> = object.get("current_test_full_name")
            .and_then(|v| v.as_str()).map(|s| s.to_string());
        Ok(IsotovideoStatusMessage::CurrentTest { name, full_name })
    }

    fn parse_paused(object: &JsonObject) -> Result<Self, anyhow::Error> {
        let reason = object.get("reason")
            .and_then(|v| v.as_str()).map(|s| s.to_string());
        Ok(IsotovideoStatusMessage::Paused { reason })
    }

    fn parse_response(object: &JsonObject) -> Result<Self, anyhow::Error> {
        let value = object.get("ret")
            .unwrap()
            .as_u64()
            .ok_or(anyhow!("Failed to convert 'ret' field to u64"))?;
        Ok(IsotovideoStatusMessage::Response { ret: value > 0 })
    }

    fn parse_stop_processing_isotovideo_commands(object: &JsonObject) -> Result<Self, anyhow::Error> {
        let value = object.get("stop_processing_isotovideo_commands")
            .ok_or(anyhow!("Missing stop_processing_isotovideo_commands field"))?
            .as_u64()
            .ok_or(anyhow!("Failed to convert stop_processing_isotovideo_commands field to u64"))?;
        Ok(IsotovideoStatusMessage::StopProcessingIsotovideoCommands { stop: value > 0 })
    }

    fn parse_stopping_test_execution(object: &JsonObject) -> Result<Self, anyhow::Error> {
        let reason = object.get("stopping_test_execution")
            .ok_or(anyhow!("Missing stopping_test_execution field"))?
            .as_str().unwrap();
        Ok(IsotovideoStatusMessage::StoppingTestExecution { reason: reason.to_string() })
    }

    pub fn from_json_object(object: &JsonObject) -> Result<Self, anyhow::Error> {
        debug!("Parsing isotovideo status message: {:?}", object);
        if object.get("current_api_function").is_some() {
            return Self::parse_current_api_function(object);
        }
        if object.get("current_test_full_name").is_some() || object.get("set_current_test").is_some() {
            return Self::parse_current_test(object);
        }
        if object.get("new_needles").is_some() {
            return Ok(IsotovideoStatusMessage::Ignored);
        }
        if object.get("paused").is_some() {
            return Self::parse_paused(object);
        }
        if object.get("ret").is_some() {
            return Self::parse_response(object);
        }
        if object.get("resume_test_execution").is_some() {
            return Ok(IsotovideoStatusMessage::ResumeTestExecution);
        }
        if object.get("stop_processing_isotovideo_commands").is_some() {
            return Self::parse_stop_processing_isotovideo_commands(object);
        }
        if object.get("stopping_test_execution").is_some() {
            return Self::parse_stopping_test_execution(object);
        }
        bail!("Unknown status message: {:?}", object);
    }

    pub fn from_websocket_message(message: &Message) -> Result<Self, anyhow::Error> {
        match message {
            Message::Text(text) => {
                match serde_json::from_str(text) {
                    Ok(JsonValue::Object(object)) => {
                        Self::from_json_object(&object)
                    },
                    Ok(_) => bail!("Expected JSON object."),
                    Err(e) => bail!("Failed to parse message as JSON: {}", e),
                }
            },
            _ => bail!("Expected text websocket message"),
        }
    }
}
