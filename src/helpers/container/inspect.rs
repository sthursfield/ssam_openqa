use anyhow::{anyhow, bail};
use process_wrap::std::*;

use std::path::Path;
use std::process::{Command, Stdio};

type JsonValue = serde_json::value::Value;
type JsonObject = serde_json::Map<String, JsonValue>;

pub fn inspect_container(command_path: &Path, name: &str) -> Result<JsonObject, anyhow::Error> {
    let mut cmd = Command::new(command_path);
    cmd.args(["container", "inspect", name]);
    cmd.stdout(Stdio::piped());
    cmd.stderr(Stdio::piped());

    let mut cmd_wrap = StdCommandWrap::from(cmd);
    cmd_wrap.wrap(ProcessGroup::leader());

    let child = cmd_wrap.spawn()?;
    let output = child.wait_with_output()?;
    if output.status.success() {
        let text = String::from_utf8_lossy(&output.stdout);
        let first_item = match serde_json::from_str(&text) {
            Ok(JsonValue::Array(values)) => {
                if values.is_empty() {
                    bail!("`container inspect` returned an empty array");
                };
                values[0].clone()
            },
            Ok(_) => bail!("`container inspect` returned a JSON value but it wasn't a list."),
            Err(e) => bail!("`container inspect` output failed to parse as JSON: {e}"),
        };
        match first_item {
            JsonValue::Object(o) => Ok(o.clone()),
            _ => bail!("`container inspect` returned a list, but it didn't contain an object"),
        }
    } else {
        let stderr_text = String::from_utf8_lossy(&output.stderr);
        bail!("Failed to inspect container: {stderr_text}");
    }
}

/// Parse `container inspect` output and return tuple `(running, exit_code)`.
pub fn parse_container_status(inspect_output: &JsonObject) -> Result<(bool, u64), anyhow::Error> {
    let state = inspect_output.get("State");
    match state {
        Some(JsonValue::Object(state_object)) => {
            let running = state_object.get("Running")
                .ok_or(anyhow!("Missing `Running` field"))?
                .as_bool()
                .ok_or(anyhow!("Failed to convert `Running` field to bool"))?;

            let exit_code = state_object.get("ExitCode")
                .ok_or(anyhow!("Missing `ExitCode` field"))?
                .as_u64()
                .ok_or(anyhow!("Failed to convert `ExitCode` field to u64"))?;

            Ok((running, exit_code))
        },
        Some(_) => bail!("Unexpected value for `State` field"),
        None => bail!("Missing `State` field"),
    }
}

