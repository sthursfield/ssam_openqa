//! Struct for specifying Podman and Docker volume mounts.

use anyhow::Context;

use std::path::{Path, PathBuf};

/// Represents a file or directory mounted from the host into the container.
#[derive(Clone)]
pub struct VolumeSpec {
    host_path: PathBuf,
    container_path: PathBuf,
}

impl VolumeSpec {
    /// Create a new VolumeSpec. The `host_path` must exist or an error will be raised.
    pub fn from_paths(host_path: &Path, container_path: &Path) -> Result<VolumeSpec, anyhow::Error> {
        let host_path = Path::new(host_path)
            .canonicalize()
            .with_context(|| format!("Host path `{}` is missing or invalid", &host_path.display()))?;

        Ok(VolumeSpec {
            host_path,
            container_path: PathBuf::from(container_path),
        })
    }

    pub fn as_container_cli_argument(&self) -> String {
        format!("--volume={}:{}", self.host_path.display(), self.container_path.display())
    }
}
