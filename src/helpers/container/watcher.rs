/// A thread that periodically polls a container for signs of life.

use log::*;

use std::path::{Path, PathBuf};
use std::thread;
use std::time::Duration;

use super::inspect::{inspect_container, parse_container_status};

pub struct Watcher {
}

impl Watcher {
    pub fn watch<F>(command_path: &Path, name: &str, mut callback: F)
    where
        F: FnMut(Result<u64, anyhow::Error>) + 'static + Send
    {
        let command_path: PathBuf = command_path.to_path_buf();
        let container_name = name.to_string();
        thread::spawn(move || {
            let interval = Duration::from_millis(500);
            loop {
                trace!("Loop polling container status");
                match inspect_container(&command_path, &container_name) {
                    Ok(inspect_output) => {
                        match parse_container_status(&inspect_output) {
                            Ok((running, exit_code)) => {
                                if !running {
                                    callback(Ok(exit_code));
                                    break;
                                }
                            },
                            Err(e) => {
                                callback(Err(e));
                                break;
                            },
                        }
                    },
                    Err(e) => {
                        callback(Err(e));
                        break;
                    },
                };
                thread::sleep(interval);
            };
            debug!("Exiting container watch thread");
        });
    }
}
