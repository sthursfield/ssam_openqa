//! Helpers for working with OCI container tools Podman and Docker.
//!
//! Visit <http://podman.io/> for more information about Podman.

pub mod inspect;
pub mod volume_spec;
pub mod watcher;

use anyhow::{anyhow, bail, Context};
use log::*;
use process_wrap::std::*;
use which::which;

use std::path::PathBuf;
use std::process::{Command, Stdio};

use self::inspect::{inspect_container};
use self::watcher::Watcher;
pub use self::volume_spec::VolumeSpec;

type JsonValue = serde_json::value::Value;
type JsonObject = serde_json::Map<String, JsonValue>;

#[derive(Clone)]
pub struct ContainerRuntime {
    command_path: PathBuf,
    process_group: ProcessGroup,
}

impl ContainerRuntime {
    /// Create a new ContainerRuntime.
    ///
    /// The `command_path` specifies an absolute path to the `podman` or `docker` program.
    ///
    /// If `command_path` is set to `None`, the first program named `podman` or `docker` found in
    /// PATH is used.
    ///
    /// The `process_group` option affects how SIGINT (CTRL+C) is handled. If the container
    /// subprocesses are in the same process group as the parent, and the user presses CTRL+C,
    /// the parent and subprocess will receive SIGINT at the same time, and they are likely to
    /// exit immediately. If the container subprocesses run in a separate process group, they
    /// will not receive SIGINT and the parent process can decide whether or not to signal them.
    pub fn new(command_path: Option<&String>, process_group: ProcessGroup) -> Result<ContainerRuntime, anyhow::Error> {
        let command_pathbuf = match command_path {
            Some(command_path) => PathBuf::from(command_path),
            None => {
                if let Ok(podman_path) = which("podman") {
                    info!("Found `podman` at {}", podman_path.display());
                    podman_path
                } else if let Ok(docker_path) = which("docker") {
                    info!("Found `docker` at {}", docker_path.display());
                    docker_path
                } else {
                    bail!("Did not find `podman` or `docker` installed.")
                }
            }
        };
        Ok(ContainerRuntime {
            command_path: command_pathbuf,
            process_group,
        })
    }

    /// Return a StdCommandWrap with the `podman` or `docker` command and the process group.
    pub fn command(&self) -> StdCommandWrap {
        let cmd = Command::new(&self.command_path);
        let mut cmd_wrap = StdCommandWrap::from(cmd);
        cmd_wrap.wrap(self.process_group);
        cmd_wrap
    }

    /// Run command with args and return the output.
    pub fn run_with_output<I,S>(&self, args: I) -> Result<std::process::Output, anyhow::Error>
    where
        I: IntoIterator<Item = S>,
        S: AsRef<std::ffi::OsStr>
    {
        let mut command = self.command();
        command.command_mut()
            .args(args)
            .stdout(Stdio::piped())
            .stderr(Stdio::piped());
        debug!("Running: {command:?}");
        let child = command.spawn()?;
        let output = child.wait_with_output()?;
        debug!("Output for {command:?}: {}", output.status);
        Ok(output)
    }

    pub fn container_exists(&self, name: &str) -> Result<bool, anyhow::Error> {
        let output = self.run_with_output(["container", "exists", name])?;
        if output.status.success() {
            Ok(true)
        } else {
            Ok(false)
        }
    }

    pub fn stop_container(&self, name: &str) -> Result<(), anyhow::Error> {
        let output = self.run_with_output(["stop", name])?;
        if output.status.success() {
            info!("Successfully stopped container {name}");
            Ok(())
        } else {
            let error_text = String::from_utf8_lossy(&output.stderr);
            Err(anyhow!("Failed to stop container {name}: {error_text}"))
        }
    }

    pub fn remove_container(&self, name: &str) -> Result<(), anyhow::Error> {
        let output = self.run_with_output(["rm", name])?;
        if output.status.success() {
            info!("Successfully removed container {name}");
            Ok(())
        } else {
            let error_text = String::from_utf8_lossy(&output.stderr);
            Err(anyhow!("Failed to remove container {name}: {error_text}"))
        }
    }

    /// Spawn a thread that periodically polls the container for signs of life.
    ///
    /// If the container exits, the thread calls `callback` and passes the return code.
    pub fn spawn_watcher<F>(&self, name: &str, callback: F)
    where
        F: FnMut(Result<u64, anyhow::Error>) + 'static + Send
    {
        Watcher::watch(&self.command_path, name, callback);
    }

    /// Returns all container configuration.
    ///
    /// The result is a JSON object whose results are defined by Podman itself.
    pub fn inspect_container(&self, name: &str) -> Result<JsonObject, anyhow::Error> {
        inspect_container(&self.command_path, name)
    }

    /// Parse the `inspect_container()` output to get a specific host port mapping.
    pub fn get_host_tcp_port(&self, inspect_result: &JsonObject, container_port: u32) -> Result<u32, anyhow::Error> {
        let network_settings = match inspect_result.get("NetworkSettings") {
            Some(JsonValue::Object(o)) => o,
            Some(_) => bail!("Required key 'NetworkSettings' was not an object, while parsing `container inspect` output."),
            None => bail!("Required key 'NetworkSettings' was missing, while parsing `container inspect` output."),
        };
        let ports = match network_settings.get("Ports") {
            Some(JsonValue::Object(o)) => o,
            Some(_) => bail!("Required key 'NetworkSettings.Ports' was not an object, while parsing `container inspect` output."),
            None => bail!("Required key 'NetworkSettings.Ports' was missing, while parsing `container inspect` output."),
        };

        let port_str = format!("{container_port}/tcp");
        let port_info = match ports.get(&port_str) {
            Some(JsonValue::Array(info_array)) => {
                if info_array.is_empty() {
                    bail!("Port info array for port {port_str} had length 0");
                };
                match &info_array[0] {
                    JsonValue::Object(first_port_info) => first_port_info.clone(),
                    _ => bail!("Port info for port {port_str} is not an object"),
                }
            }
            Some(_) => bail!("Port info for port {port_str} was not an object"),
            None => bail!("Port {port_str} is not listed in container's port mappings."),
        };
        let host_port = port_info.get("HostPort");
        match host_port {
            Some(JsonValue::String(host_port)) => Ok(host_port.parse()?),
            Some(JsonValue::Number(host_port)) => {
                let host_port_u64 = host_port.as_u64()
                    .context("Host port couldn't be converted to u64")?;
                Ok(host_port_u64 as u32)
            },
            Some(_) => bail!("Required key 'HostPort' was not a string, for port {port_str}. Got: {host_port:?}."),
            None => bail!("Required key 'HostPort' missing from info about port {port_str}"),
        }
    }
}
