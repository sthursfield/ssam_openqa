//! Load the OpenQA scenario definitions from a YAML file.

use std::collections::{BTreeMap, HashMap};
use std::fs;
use std::path::Path;

use anyhow::{self, Context};
use serde::{Deserialize};

use crate::helpers::isotovideo::IsotovideoConfig;

#[derive(Deserialize,PartialEq,Debug)]
pub struct ProductDefinition {
    pub distri: String,
    pub flavor: String,
    pub version: String,
    pub arch: String,
}

#[derive(Deserialize,PartialEq,Debug)]
pub struct MachineDefinition {
    pub backend: String,
    pub settings: HashMap<String, String>,
}

#[derive(Deserialize,PartialEq,Debug)]
pub struct JobTemplateDefinition {
    pub product: String,
    pub machine: String,
    pub settings: HashMap<String, String>,
}

#[derive(Deserialize,PartialEq,Debug)]
pub struct ScenarioDefinitions {
    pub products: HashMap<String, ProductDefinition>,
    pub machines: HashMap<String, MachineDefinition>,
    pub job_templates: BTreeMap<String, JobTemplateDefinition>,
}


impl ProductDefinition {
    /// Set isotovideo config variables for this product.
    pub fn set_isotovideo_config(&self, config: &mut IsotovideoConfig) -> Result<(), anyhow::Error> {
        config.add_key("DISTRI", &self.distri)?;
        config.add_key("FLAVOR", &self.flavor)?;
        config.add_key("VERSION", &self.version)?;
        config.add_key("ARCH", &self.arch)?;
        Ok(())
    }
}

impl MachineDefinition {
    /// Set isotovideo config variables for this product.
    pub fn set_isotovideo_config(&self, config: &mut IsotovideoConfig) -> Result<(), anyhow::Error> {
        config.add_key("BACKEND", &self.backend)?;
        for (key, value) in self.settings.iter() {
            config.add_key(key, value)?;
        };
        Ok(())
    }
}

impl ScenarioDefinitions {
    /// Load the OpenQA test configuration from a `scenario_definitions.yaml` file.
    pub fn load_from_file(file: &Path) -> Result<Self, anyhow::Error> {
        let text = fs::read_to_string(file)?;
        let this = serde_yaml::from_str(&text)?;
        Ok(this)
    }

    pub fn get_job_template(&self, name: &str) -> Result<&JobTemplateDefinition, anyhow::Error> {
        let job_template = self.job_templates.get(name)
            .context(format!("Job template {name} is not defined"))?;
        Ok(job_template)
    }

    /// Tests all testsuites (job templates) defined in the scenarios YAML.
    pub fn get_testsuites(&self) -> Vec<String> {
        self.job_templates.keys().map(|s| s.to_owned()).collect()
    }

    /// Construct an isotovideo config to run the given testsuite.
    pub fn get_isotovideo_config_for_testsuite(
        &self,
        testsuite_name: &str,
        extra_vars: &HashMap<String, String>
    ) -> Result<IsotovideoConfig, anyhow::Error> {
        let mut config = IsotovideoConfig::new();

        let job_template = self.get_job_template(testsuite_name)?;

        let product_name = &job_template.product;
        let product = self.products.get(product_name)
            .context(format!("Unknown product {product_name}"))?;
        product.set_isotovideo_config(&mut config)?;

        let machine_name = &job_template.machine;
        let machine = self.machines.get(machine_name)
            .context(format!("Unknown machine {machine_name}"))?;
        machine.set_isotovideo_config(&mut config)?;

        config.update(&job_template.settings);
        config.update(extra_vars);

        config.add_key("TEST", testsuite_name)?;

        Ok(config)
    }
}
