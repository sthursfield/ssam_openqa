//! Pause events are specific types of breakpoint that the openQA test runner makes available.
//!
//! These correspond to the following os-autoinst
//! [backend variables](https://github.com/os-autoinst/os-autoinst/blob/master/doc/backend_vars.asciidoc):
//!
//!   * `PAUSE_ON_SCREEN_MISMATCH`
//!   * `PAUSE_ON_NEXT_COMMAND`
//!   * `PAUSE_ON_FAILURE`

use anyhow::bail;

use std::fmt;
use std::str::FromStr;

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub enum PauseEvent {
    #[default]
    /// Don't pause.
    None,
    /// Pause on any kind of test failure.
    Failure,
    /// Pause if check_screen() or assert_screen() fail.
    ScreenMismatch,
    /// Pause if assert_screen() fails.
    AssertScreenMismatch,
    /// Pause on every test API command.
    Command,
}

impl PauseEvent {
    /// Return the "next" pause event
    ///
    /// This is used when stepping through possible configurations in the interactive frontend
    pub fn next(&self) -> PauseEvent {
        match self {
            PauseEvent::None => PauseEvent::Failure,
            PauseEvent::Failure => PauseEvent::ScreenMismatch,
            PauseEvent::ScreenMismatch => PauseEvent::AssertScreenMismatch,
            PauseEvent::AssertScreenMismatch => PauseEvent::Command,
            PauseEvent::Command => PauseEvent::None,
        }
    }
}

impl fmt::Display for PauseEvent {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let name = match self {
            PauseEvent::None => "none",
            PauseEvent::Failure => "failure",
            PauseEvent::ScreenMismatch => "screen-mismatch",
            PauseEvent::AssertScreenMismatch => "assert-screen-mismatch",
            PauseEvent::Command => "command",
        };
        write!(f, "{}", name)
    }
}

impl FromStr for PauseEvent {
    type Err = anyhow::Error;

    fn from_str(input: &str) -> Result<PauseEvent, anyhow::Error> {
        match input {
            "failure" => Ok(PauseEvent::Failure),
            "screen-mismatch" => Ok(PauseEvent::ScreenMismatch),
            "assert-screen-mismatch" => Ok(PauseEvent::AssertScreenMismatch),
            "command" => Ok(PauseEvent::Command),
            _ => bail!("Unknown pause event type: {}.", input)
        }
    }
}
