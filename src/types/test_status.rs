use serde::Serialize;

#[derive(Copy, Clone, Debug, PartialEq, Serialize)]
pub enum TestStatus {
    Pass,
    Fail,
    Abort,
}
