//! Configuration for a test run which the user can change during the test.

use crate::types::PauseEvent;

#[derive(Clone, Debug)]
pub struct UserConfig {
    pub pause_at_test: String,
    pub pause_on_event: PauseEvent,
}
