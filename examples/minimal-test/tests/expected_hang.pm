use base 'basetest';

use strict;
use warnings;
use testapi;

sub run {
    select_console("root-virtio-terminal");
    assert_script_run("sleep 100");
}

1;
