use strict;
use warnings;
use autotest;
use testapi;

my $distri = testapi::get_required_var('CASEDIR') . '/lib/minimaldistribution.pm';
require $distri;
testapi::set_distribution(minimaldistribution->new);

$testapi::username = "root";
$testapi::password = "insecure";

my $testsuite = testapi::get_required_var('TEST');

if ($testsuite eq 'minimal_test') {
    autotest::loadtest("tests/minimal.pm");
} elsif ($testsuite eq 'minimal_test_expected_fail') {
    # This is used by integration tests.
    autotest::loadtest("tests/expected_fail.pm");
} elsif ($testsuite eq 'minimal_test_expected_hang') {
    # This is used by integration tests.
    autotest::loadtest("tests/expected_hang.pm");
} else {
    die ("Invalid testsuite: '$testsuite'");
};

1;
