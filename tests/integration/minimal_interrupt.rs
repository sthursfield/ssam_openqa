//! Test CTRL+C handling in ssam_openqa.

use nix::sys::signal::Signal;
use nix::unistd::Pid;
use serde_json::json;

use std::time::Duration;

use crate::fixtures::{RealScenario, FrontendMessage};

/// Scenario in which testsuite hangs and is interrupted.
///
/// This test waits until the test_order is loaded before interrupting.
#[test]
#[ignore]
fn test_minimal_interrupt_after_test_order() {
    let mut fixture = RealScenario::new("minimal_test_expected_hang");

    println!("Run until `test_order` message is received");
    fixture.add_message_callback(
        Box::new(|message: &FrontendMessage, child_pid: &Pid| {
            if *message == json!({"TestOrder":["expected_hang"]}) {
                nix::sys::signal::kill(*child_pid, Signal::SIGINT)
                    .expect("failed to send signal to child process");
            }
        })
    );

    let (mut child, output) = fixture.run(Duration::from_secs(10));

    println!("Waiting for child to exit");
    child.wait().unwrap();

    if output.timeout {
        panic!("Timeout waiting for expected message");
    } else {
        println!("Child exited, got {} messages", output.message_events.len());
    }

    /* Show full process output */
    output.print_messages_to_stdout();

    output.assert_frontend_message(
        &json!({"IsotovideoContainerStart":"ssam_openqa_minimal_test_expected_hang"}),
    );
    output.assert_frontend_message(
        &json!({"ProgramExit": {"error": null, "testsuite_result": "Abort"}})
    );
}
