use assert_cmd::Command;

/// Test that the `--help` CLI argument works.
#[test]
fn test_cli_help() {
    let mut cmd = Command::cargo_bin("ssam_openqa").unwrap();
    cmd.arg("--help");
    cmd.assert().success();
}
