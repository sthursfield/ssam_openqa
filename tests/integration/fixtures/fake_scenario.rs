//! Fixture providing a fake openQA scenario generated in code.

use std::fs::{DirBuilder, File};
use std::io::Write;
use std::path::PathBuf;

use assert_cmd::Command;
use tempfile::{tempdir, TempDir};

pub struct FakeScenario {
    work_dir: TempDir,
    output_dir: PathBuf,
}

/// Fixture providing a fake openQA scenario.
impl FakeScenario {
    pub fn new() -> Self {
        let work_dir = tempdir().unwrap();

        let mut output_dir = PathBuf::from(work_dir.path());
        output_dir.push("output");

        let dir_builder = DirBuilder::new();
        dir_builder.create(work_dir.path().join("config")).unwrap();

        Self {
            work_dir,
            output_dir,
        }
    }

    /// Create the fake scenario_definitions.yaml file.
    pub fn write_scenario_definitions(&self) {
        let scenario_definitions_path = self.work_dir.path().join("config/scenario_definitions.yaml");

        let mut scenario_definitions_file = File::create(scenario_definitions_path).unwrap();
        writeln!(scenario_definitions_file, "products:").unwrap();
        writeln!(scenario_definitions_file, "  test_product:").unwrap();
        writeln!(scenario_definitions_file, "    distri: testos").unwrap();
        writeln!(scenario_definitions_file, "    flavor: iso").unwrap();
        writeln!(scenario_definitions_file, "    version: master").unwrap();
        writeln!(scenario_definitions_file, "    arch: x86_64").unwrap();
        writeln!(scenario_definitions_file, "machines:").unwrap();
        writeln!(scenario_definitions_file, "  qemu_x86_64:").unwrap();
        writeln!(scenario_definitions_file, "    backend: none").unwrap();
        writeln!(scenario_definitions_file, "    settings:").unwrap();
        writeln!(scenario_definitions_file, "job_templates:").unwrap();
        writeln!(scenario_definitions_file, "  test:").unwrap();
        writeln!(scenario_definitions_file, "    product: test_product").unwrap();
        writeln!(scenario_definitions_file, "    machine: qemu_x86_64").unwrap();
        writeln!(scenario_definitions_file, "    settings:").unwrap();
    }

    /// Run the ssam_openqa tool on the fake scenario.
    pub fn cmd(&self) -> Command {
        let mut cmd = Command::cargo_bin("ssam_openqa").unwrap();
        cmd.args([
            "run",
            "--frontend", "json",
            "--tests-path", self.work_dir.path().to_str().unwrap(),
            "--output-path", self.output_dir.to_str().unwrap()
        ]);
        cmd
    }
}
