//! Tests that run isotovideo for real, using `examples/minimal-test`.

// To provide Command::cargo_bin().
use assert_cmd::prelude::*;
use chrono::Local;
use log::*;
use nix::sys::signal::Signal;
use nix::unistd::Pid;
use tempfile::{tempdir, TempDir};

use std::env;
use std::io::Read;
use std::path::{Path, PathBuf};
use std::process::{Command, Child, Stdio};
use std::time::{Duration, Instant};

use super::{FrontendMessage, FrontendMessageEvent, FrontendMessageList, frontend_message_exists, MessageMatchRule};

/// Path to `examples/minimal-test` scenario.
pub fn minimal_test_scenario_path() -> PathBuf {
    let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    let mut path = PathBuf::new();
    path.push(manifest_dir);
    path.push("examples/minimal-test");
    path
}

type Message = serde_json::value::Value;

/// Callback type which tests can use to trigger events (e.g. SIGINT)
/// when a specific message is received.
type MessageCallback = dyn Fn(&Message, &Pid);

/// Fixture to run a real scenario test.
pub struct RealScenario {
    // Path to the ISO and openQA test definition.
    scenario_dir: PathBuf,
    // Directory for test runner output.
    output_dir: TempDir,
    // Command to launch test runner.
    command: Command,
    // Message callback
    message_callbacks: Vec<Box<MessageCallback>>,
}

/// Result of running the scenario.
pub struct Output {
    // Messages received from frontend, with timestamps.
    pub message_events: FrontendMessageList,
    // True if the `run` function exited due to timeout.
    // Note it does not kill the child process in this case.
    pub timeout: bool,
}

impl Output {
    /// Ensure `child` is stopped, and panic in case of error or timeout.
    pub fn stop_and_assert_ok(&self, child: &mut Child) {
        if self.timeout {
            println!("Sending SIGTERM to child");
            nix::sys::signal::kill(Pid::from_raw(child.id() as i32), Signal::SIGTERM)
                .expect("cannot send SIGTERM");
            panic!("Test hit timeout.");
        }

        let exit_status = child.wait().unwrap();
        assert!(exit_status.success());
    }

    pub fn print_messages_to_stdout(&self) {
        for message in &self.message_events {
            println!("{}", message);
        }
    }

    /// Assert that `expected` was received.
    pub fn assert_frontend_message(&self, expected: &FrontendMessage) {
        if !frontend_message_exists(&self.message_events, expected, MessageMatchRule::Identical) {
            panic!("Did not find expected message: {}. Recorded messages: {}", expected, self.message_events.len());
        }
    }

    /// Assert that `expected` was received, but don't fail if the matched message has extra keys.
    ///
    /// This allows you to ignore timestamps and complex fields.
    pub fn assert_frontend_message_partial(&self, expected: &FrontendMessage) {
        if !frontend_message_exists(&self.message_events, expected, MessageMatchRule::AllowExtraKeys) {
            panic!("Did not find expected message: {}. Recorded messages: {}", expected, self.message_events.len());
        }
    }
}

/// Process a byte read from child stdout.
///
/// Since the buffer does not break on line boundries, current_line is passed
/// back to the caller each time.
///
/// Returns `Some(String)` on newline, and `None` otherwise.
fn process_stdout_byte(byte: &u8, current_line: &mut Vec<u8>) -> Option<String> {
    let mut line: Option<String> = None;
    match byte {
        b'\n' => {
            line = Some(String::from_utf8(current_line.clone()).unwrap());
            current_line.clear();
        },
        _ => { current_line.push(*byte) },
    }
    line
}

/// Command to run the ssam_openqa tool for the real scenario.
fn build_command(scenario_dir: &Path, output_dir: &Path, testsuite_name: &str) -> Command {
    let mut cmd = Command::cargo_bin("ssam_openqa").unwrap();

    let mut output_path = PathBuf::new();
    output_path.push(output_dir);
    output_path.push("out");

    let mut iso_path = PathBuf::new();
    iso_path.push(scenario_dir);
    iso_path.push("buildroot-minimal.iso");

    cmd.args([
        "run",
        "--frontend", "json",
        "--tests-path", scenario_dir.to_str().unwrap(),
        "--output-path", output_path.to_str().unwrap(),
        "--iso-path", iso_path.to_str().unwrap(),
        "--testsuites", testsuite_name,
    ]);

    cmd.stdout(Stdio::piped());

    cmd
}

impl RealScenario {
    pub fn new(testsuite_name: &str) -> Self {
        let scenario_dir = minimal_test_scenario_path();
        let output_dir = tempdir().unwrap();
        let command = build_command(scenario_dir.as_path(), output_dir.path(), testsuite_name);

        Self { scenario_dir, output_dir, command, message_callbacks: Vec::new() }
    }

    pub fn command(&mut self) -> &mut Command {
        &mut self.command
    }

    pub fn add_message_callback(&mut self, message_callback: Box::<MessageCallback>) {
        self.message_callbacks.push(message_callback);
    }

    /// Run the scenario until completion, timeout, or a specific message.
    ///
    /// Arguments:
    ///
    ///   * `timeout`: runner will return Err() if this is exceeded.
    ///   * `stop_at_message`: if set, the function returns as soon as a specific message is received.
    pub fn run(self: &mut Self, timeout: Duration) -> (Child, Output) {
        info!("Starting child process");
        let mut child = self.command.spawn().unwrap();
        let child_pid = Pid::from_raw(child.id() as i32);
        let child_stdout = child.stdout.as_mut().unwrap();

        let mut current_line: Vec<u8> = Vec::new();
        let mut messages: FrontendMessageList = Vec::new();
        let mut hit_timeout = false;

        let start_time = Instant::now();

        loop {
            let mut buffer: [u8;1] = [0;1];
            let n_bytes_read = child_stdout.read(&mut buffer).unwrap();
            debug!("Read {} bytes from child stdout", n_bytes_read);

            if n_bytes_read == 0 {
                info!("Child stdout closed, exiting read loop");
                break;
            }

            let new_line = process_stdout_byte(&buffer[0], &mut current_line);
            if let Some(line) = new_line {
                let message_event = FrontendMessageEvent::from_text(Local::now(), &line);
                debug!("Parsed message: {:?}", message_event);

                // Tests can trigger actions during ssam_openqa execution based on messages.
                for message_callback in &self.message_callbacks {
                    message_callback(&message_event.message, &child_pid);
                }

                messages.push(message_event);
            }

            if Instant::now() >= start_time + timeout {
                hit_timeout = true;
                break;
            }
        }

        (child, Output { message_events: messages, timeout: hit_timeout })
    }
}
