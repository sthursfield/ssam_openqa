/// Helpers for messages received from the frontend during integration tests.

use chrono::{DateTime, Local};

use std::fmt;

/// A single message, which is a JSON value.
pub type FrontendMessage = serde_json::value::Value;

// Parse a single line to a single message.
fn parse_frontend_message(line: &str) -> FrontendMessage {
    match serde_json::from_str(line) {
        Ok(message) => {
            match message {
                serde_json::value::Value::Object(_) => message,
                _ => { panic!("Unexpected JSON value output by JSON frontend: {}", message) },
            }
        },
        Err(e) => {
            panic!("Unable to parse line as JSON: `{}`. Error: {}.", line, e);
        },
    }
}

/// A MessageEvent is the message + time it was received.
#[derive(Debug)]
pub struct FrontendMessageEvent {
    pub instant: DateTime<Local>,
    pub message: FrontendMessage,
}

impl FrontendMessageEvent {
    /// Deserialize a frontend message and attach the given timestamp to create an Event.
    pub fn from_text(instant: DateTime<Local>, line: &str) -> Self {
        let message = parse_frontend_message(&line);
        Self { instant, message }
    }
}

impl fmt::Display for FrontendMessageEvent {
    // Pretty printer for message events.
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}: {}", self.instant.format("%H:%M:%S").to_string(), self.message)
    }
}

/// A list of message events.
pub type FrontendMessageList = Vec<FrontendMessageEvent>;

/// Match strategies for frontend_message_exists().
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum MessageMatchRule {
    // The message must be exactly what was expected.
    Identical,
    // The message must contain the expected keys, and may contain extra keys as well.
    // Useful for ignoring certain fields such as timestamps.
    AllowExtraKeys,
}

fn match_message(message: &FrontendMessage, expected: &FrontendMessage, match_rule: MessageMatchRule) -> bool {
    if match_rule == MessageMatchRule::Identical {
        message == expected
    } else if match_rule == MessageMatchRule::AllowExtraKeys {
        match (message, expected) {
            (FrontendMessage::Object(message), FrontendMessage::Object(expected)) => {
                // For objects, match that the keys in `expected` are all present in `message.`
                // Additional keys in `message` are ignored here and won't raise an error.
                expected.iter().all(|(key, expected_value)| {
                    message.get(key).map_or(
                        false,
                        |message_value| match_message(message_value, expected_value, match_rule)
                    )
                })
            },
            _ => message == expected,
        }
    } else {
        panic!("Invalid match rule");
    }
}

/// Return `true` if a message that exactly matches `expected` is present in `messages`.
pub fn frontend_message_exists(messages: &FrontendMessageList, expected: &FrontendMessage, match_rule: MessageMatchRule) -> bool {
    for message_event in messages {
        if match_message(&message_event.message, expected, match_rule) {
            return true;
        }
    }
    false
}
