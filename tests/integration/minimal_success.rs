use serde_json::json;

use std::time::Duration;

use crate::fixtures::RealScenario;

/// Scenario in which testsuite passes.
#[test]
#[ignore]
fn test_minimal_success() {
    let mut fixture = RealScenario::new("minimal_test");

    let (mut child, output) = fixture.run(Duration::from_secs(60));

    output.stop_and_assert_ok(&mut child);

    /* Show full process output */
    output.print_messages_to_stdout();

    output.assert_frontend_message(
        &json!({"IsotovideoContainerStart":"ssam_openqa_minimal_test"}),
    );
    output.assert_frontend_message(&json!({"TestOrder":["minimal"]}));
    output.assert_frontend_message(
        &json!({"IsotovideoStatus":{"CurrentTest":{"name":"minimal","full_name":"tests-minimal"}}}),
    );
    output.assert_frontend_message_partial(
        &json!(
            {"TestResult":
                {
                    "test_name":"minimal",
                    // We don't match the full test results, just "result"
                    "test_result": { "result": "ok" }
                }
            }
        ),
    );
    output.assert_frontend_message(&json!({"IsotovideoContainerExit":0}));
    output.assert_frontend_message(
        &json!({"TestsuiteResult":{"testsuite_result":"Pass"}}),
    );
    output.assert_frontend_message(
        &json!({"ProgramExit":{"testsuite_result":"Pass","error":null}}),
    );
}
