use predicates::prelude::*;

use crate::fixtures::FakeScenario;

/// Test that bad Podman or Docker installation is detected and reported to user.
#[test]
fn test_bad_container_runtime() {
    let fixture = FakeScenario::new();
    fixture.write_scenario_definitions();

    let mut cmd = fixture.cmd();
    cmd.env("DOCKER", "/bin/false");
    cmd.assert().failure()
        .stdout(predicate::str::contains("/bin/false returned code 1."))
        .stdout(predicate::str::contains("Error: [none]"));
}
