use std::path::PathBuf;

use assert_cmd::Command;
use predicates::prelude::*;
use tempfile::tempdir;

/// Test that running with no `scenario_definitions.yaml` raises error.
#[test]
fn test_run_error_missing_test_config() {
    let work_dir = tempdir().unwrap();

    let mut output_dir = PathBuf::from(work_dir.path());
    output_dir.push("output");

    let mut cmd = Command::cargo_bin("ssam_openqa").unwrap();
    cmd.args(["run", "--frontend", "json", "--tests-path", work_dir.path().to_str().unwrap(), "--output-path", output_dir.to_str().unwrap()]);
    cmd.env("DOCKER", "/bin/true");
    cmd.assert().failure()
        .stdout(predicate::str::contains("Failed to load scenario definitions"));
}

