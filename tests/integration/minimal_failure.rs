mod fixtures;

use assert_cmd::prelude::*;

use serde_json::json;

use fixtures::{assert_frontend_message, parse_json_frontend_output, RealScenario};

/// Scenario in which testsuite fails.
#[test]
#[ignore]
fn test_minimal_failure() {
    let fixture = RealScenario::new();

    let mut cmd = fixture.cmd("minimal_test_expected_fail");
    let cmd_result = cmd.assert();

    let messages = parse_json_frontend_output(&cmd_result.get_output().stdout);

    for message in &messages {
        println!("{}", message);
    }

    assert_frontend_message(
        &messages,
        &json!({"IsotovideoContainerStart":"ssam_openqa_minimal_test_expected_fail"}),
    );
    assert_frontend_message(&messages, &json!({"TestOrder":["expected_fail"]}));
    assert_frontend_message(
        &messages,
        &json!({"TestStatus":{"test_name":"expected_fail","test_status":"Fail"}}),
    );
    assert_frontend_message(&messages, &json!({"IsotovideoContainerExit":0}));
    assert_frontend_message(
        &messages,
        &json!({"TestsuiteResult":{"testsuite_result":"Fail"}}),
    );
    assert_frontend_message(
        &messages,
        &json!({"ProgramExit":{"testsuite_result":"Fail","error":null}}),
    );
}
