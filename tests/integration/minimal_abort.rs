use serde_json::json;

use std::time::Duration;

use crate::fixtures::RealScenario;

/// Scenario in which connection to isotovideo control socket fails.
#[test]
#[ignore]
fn test_minimal_abort() {
    let mut fixture = RealScenario::new("minimal_test_expected_fail");

    fixture.command()
        .env("_SSAM_OPENQA_WEBSOCKET_URL", "http://fault-injection-invalid-url");

    let (mut child, output) = fixture.run(Duration::from_secs(60));

    output.stop_and_assert_ok(&mut child);

    /* Show full process output */
    output.print_messages_to_stdout();

    output.assert_frontend_message(
        &json!({"IsotovideoContainerStart":"ssam_openqa_minimal_test_expected_fail"}),
    );
    //assert_frontend_message(&messages, &json!({"TestOrder":["expected_fail"]}));
    output.assert_frontend_message(
        &json!({"IsotovideoStatus":{"ConnectionFailed":{"error": "connect_async(http://fault-injection-invalid-url) failed: URL error: URL scheme not supported"}}}),
    );
    output.assert_frontend_message(
        &json!({"ProgramExit":{"testsuite_result":"Abort","error":null}}),
    );
}
